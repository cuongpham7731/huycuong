<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBooksTable extends Migration {

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up() {
        Schema::create('books', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->text('description');
            $table->unsignedInteger('language_id');
            $table->unsignedInteger('author_id');
            $table->unsignedInteger('publisher_id');
            $table->unsignedInteger('type_id');
            $table->integer('page');
            $table->date('publication_date');
            $table->string('dimensions');
            $table->decimal('origin_price', 12, 2);
            $table->decimal('promotion_price', 12, 2);
            $table->decimal('final_price', 12, 2);
            $table->integer('origin');
            $table->integer('remain');
            $table->tinyInteger('is_thumbnail')->default('0');
            $table->timestamps();
            $table->foreign('language_id')->references('id')->on('languages')->onDelete('cascade');
            $table->foreign('author_id')->references('id')->on('authors')->onDelete('cascade');
            $table->foreign('publisher_id')->references('id')->on('publishers')->onDelete('cascade');
            $table->foreign('type_id')->references('id')->on('types')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down() {
        Schema::dropIfExists('books');
    }

}
