<?php

use Illuminate\Database\Seeder;
use Faker\Factory as Faker;

class AuthorsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
         if (DB::table('authors')->get()->count() == 0) {
            $authors = [
                [
                    'name' => 'Nguyễn Nhật Ánh',
                    'image' => 'images/Authors/nnanh.jpg',
                    'introduce' =>"Nguyễn Nhật Ánh là nhà văn Việt Nam chuyên viết cho tuổi mới lớn. Ông sinh ngày 7 tháng 5 năm 1955 tại làng Đo Đo, xã Bình Quế, huyện Thăng Bình, tỉnh Quảng Nam.",
                    'created_at' => DB::raw('now()'),
                    'updated_at' => DB::raw('now()'),
                ],[
                    'name' => 'Trang Hạ',
                    'image' => 'images/Authors/tha.jpg',
                    'introduce' =>'Nhà văn Trang Hạ sinh ngày 30-11-1975 tại Thành phố Hà Nội, nước Việt Nam. Bà sinh thuộc cung Nhân Mã, cầm tinh con (giáp) mèo (Ất Mão 1975). Trang Hạ xếp hạng nổi tiếng thứ 463 trên thế giới và thứ 4 trong danh sách Nhà văn nổi tiếng. Tổng dân số của Việt Nam năm 1975 vào khoảng 48,03 triệu người.',
                    'created_at' => DB::raw('now()'),
                    'updated_at' => DB::raw('now()'),
                ],[
                    'name' => 'Anh Khang',
                    'image' => 'images/Authors/akhang.jpg',
                    'introduce' =>'Sớm định hình phong cách riêng trong dòng văn học trẻ sôi động, Anh Khang - tác giả sinh năm 1987 được độc giả trẻ đón nhận qua những tựa sách “hot” như “Ngày trôi về phía cũ”, “Đường hai ngả người thương thành lạ” hay “Buồn làm sao buông”.',
                    'created_at' => DB::raw('now()'),
                    'updated_at' => DB::raw('now()'),
                ],[
                    'name' => 'Sơn Paris',
                    'image' => 'images/Authors/sparis.jpg',
                    'introduce' =>'Chàng tác giả sinh năm 1994 đã phải vượt qua nhiều dư luận trên mạng xã hội để có được vị trí như bây giờ. Sách của Sơn Paris luôn nằm trong những tác phẩm được đón đợi nhất, đặc biệt phù hợp với lứa tuổi học sinh và độc giả nữ.',
                    'created_at' => DB::raw('now()'),
                    'updated_at' => DB::raw('now()'),
                ],
                [
                    'name' => 'Nguyễn Ngọc Thạch',
                    'image' => 'images/Authors/nnthach.jpg',
                    'introduce' =>'Những tác phẩm “Đời Callboy”, hay “Chênh vênh 25” của nhà văn sinh 28 tuổi này được bạn đọc nhiệt tình đón nhận. Việc tạo dựng một phong cách riêng cho mình có vẻ đã đem đến cho Nguyễn Ngọc Thạch những thành công nhất định.',
                    'created_at' => DB::raw('now()'),
                    'updated_at' => DB::raw('now()'),
                ],[
                    'name' => 'Emily Bronte',
                    'image' => 'images/Authors/EmilyBronte.jpg',
                    'introduce' =>'Emily Brontë was born on 30 July 1818 in the village of Thornton Market Street on the outskirts of Bradford, in the West Riding of Yorkshire, in Northern England, to Maria Branwell and an Irish father, Patrick Brontë. She was the younger sister of Charlotte Brontë and the fifth of six children. In 1820, shortly after,the birth of Emilys younger sister Anne, the family moved eight miles away to Haworth, where Patrick was employed as perpetual curate; here the children developed their literary talents',
                    'created_at' => DB::raw('now()'),
                    'updated_at' => DB::raw('now()'),
                ],[
                    'name' => 'Haruki Murakami',
                    'image' => 'images/Authors/HarukiMurakami.jpg',
                    'introduce' =>'Haruki Murakami is a Japanese writer. His books and stories have been bestsellers in Japan as well as internationally, with his work being translated into 50 languages[1] and selling millions of copies outside his native country',
                    'created_at' => DB::raw('now()'),
                    'updated_at' => DB::raw('now()'),
                ]
                ];
            DB::table('authors')->insert($authors);
         }
    }
}
