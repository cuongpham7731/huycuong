<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Faker\Factory as Faker;

class CommentsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Faker::create();
    	foreach (range(1,50) as $index) {
	        DB::table('comments')->insert([
	            'content' => $faker->paragraph,
                    'user_id' => 0,
                    'parent_id' => 0,
                    'book_id' => $faker->numberBetween(1,50),
	        ]);
	}
    }
}
