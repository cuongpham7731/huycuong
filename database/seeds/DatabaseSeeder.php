<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder {

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run() {

        $this->call([
            UsersTableSeeder::class,
            LanguagesTableSeeder::class,
            AuthorsTableSeeder::class,
            PublishersTableSeeder::class,
            TypesTableSeeder::class,
            BooksTableSeeder::class,
            Pictures_TableSeeder::class,
            WishlistsTableSeeder::class,
            OrdersTableSeeder::class,
            Order_ProductsTableSeeder::class,
            CommentsTableSeeder::class,
            
        ]);
    }

}
