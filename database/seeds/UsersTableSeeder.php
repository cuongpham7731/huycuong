<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Faker\Factory as Faker;

class UsersTableSeeder extends Seeder {

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run() {
        $faker = Faker::create();
        if (DB::table('users')->get()->count() == 0) {
            $users = [
                [
                    'name' => 'dong nguyen',
                    'email' => 'dong.nguyen@gmail.com',
                    'password' => bcrypt('secret'),
                    'address' => $faker->address,
                    'phone' => $faker->phoneNumber,
                    'image' => 'images/demo/user/1.jpg',
                ],[
                    'name' => 'vy phan',
                    'email' => 'vy.phan@gmail.com',
                    'password' => bcrypt('secret'),
                    'address' => $faker->address,
                    'phone' => $faker->phoneNumber,
                    'image' => 'images/demo/user/4.jpg',
                ],[
                    'name' => 'thao dinh',
                    'email' => 'thao.dinh@gmail.com',
                    'password' => bcrypt('secret'),
                    'address' => $faker->address,
                    'phone' => $faker->phoneNumber,
                    'image' => 'images/demo/user/3.jpg',
                ],[
                    'name' => 'cuong pham',
                    'email' => 'cuong.pham@gmail.com',
                    'password' => bcrypt('secret'),
                    'address' => $faker->address,
                    'phone' => $faker->phoneNumber,
                    'image' => 'images/demo/user/2.jpg',
                ],
            ];
             DB::table('users')->insert($users);
        }
    }

}
