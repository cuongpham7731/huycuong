<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Faker\Factory as Faker;

class OrdersTableSeeder extends Seeder {

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run() {
        if (DB::table('orders')->get()->count() == 0) {
            $faker = Faker::create();
            foreach (range(1, 50) as $index) {
                DB::table('orders')->insert([
                    'order_date' => '2018-03-18',
                    'shipper_date' => '2018-03-20',
                    'email' => $faker->email,
                    'name' => $faker->name,
                    'address' => $faker->address,
                    'phone' => $faker->phoneNumber,
                    'user_id' => $faker->numberBetween(1,4),
                ]);
            }
        }
    }

}
