CREATE DATABASE book_sales;
USE book_sales;

CREATE TABLE languages(
id INT(11) AUTO_INCREMENT NOT NULL,
name VARCHAR(255) NOT NULL,
is_thumbnail tinyint NOT NULL,
PRIMARY KEY(id)
)CHARACTER SET = utf8 , COLLATE = utf8_unicode_ci ;

INSERT INTO languages(name,is_thumbnail)
VALUES 	('Tiếng Việt',1),
		('Tiếng Anh',1);
        
SELECT * FROM languages;

CREATE TABLE authors(
id INT(11) AUTO_INCREMENT NOT NULL,
name VARCHAR(255) NOT NULL,
status tinyint NOT NULL,
is_thumbnail tinyint NOT NULL,
PRIMARY KEY(id)
)CHARACTER SET = utf8 , COLLATE = utf8_unicode_ci ;

INSERT INTO authors(name, is_thumbnail)
VALUES 	('Nguyễn Nhật Ánh', 1),
		('Hồ Chí Minh', 1),
        ('Nam Cao', 1);
        
SELECT * FROM authors;

CREATE TABLE publishers(
id INT(11) AUTO_INCREMENT NOT NULL,
name VARCHAR(255) NOT NULL,
is_thumbnail tinyint NOT NULL,
PRIMARY KEY(id)
)CHARACTER SET = utf8 , COLLATE = utf8_unicode_ci ;

INSERT INTO publishers(name,is_thumbnail)
VALUES 	('Nguyễn Thị Đông',1),
		('Đinh Thị Thảo',1),
        ('Phan Thảo Vy',1);

SELECT * FROM publishers;
        
CREATE TABLE types(
id INT(11) AUTO_INCREMENT NOT NULL,
name VARCHAR(255) NOT NULL,
is_thumbnail tinyint NOT NULL,
PRIMARY KEY (id)
)CHARACTER SET = utf8 , COLLATE = utf8_unicode_ci ;

INSERT INTO types(name,is_thumbnail)
VALUES 	('Tiểu thuyết',1),
		('Tiểu thuyết',1),
        ('Truyện',1);
SELECT * FROM types;

CREATE TABLE books(
id INT(11) AUTO_INCREMENT NOT NULL,
name VARCHAR (255) NOT NULL,
description VARCHAR(255) NOT NULL,
language_id INT(11) ,
author_id INT(11) ,
publisher_id INT(11) ,
type_id INT(11) ,
is_thumbnail tinyint NOT NULL,
PRIMARY KEY(id),
FOREIGN KEY(language_id) REFERENCES languages(id),
FOREIGN KEY(author_id) REFERENCES authors(id),
FOREIGN KEY(publisher_id) REFERENCES publishers(id),
FOREIGN KEY(type_id) REFERENCES types(id)
)CHARACTER SET = utf8 , COLLATE = utf8_unicode_ci ;
INSERT INTO books( name, description, language_id, author_id, publisher_id, type_id,is_thumbnail)
VALUES 	('Cho tôi xin một vé đi tuổi thơ','Tác phẩm nổi tiếng  từ năm 1945, Nguyễn Nhật Ánh đã gửi hết thời thơ ấu của mình qua tác phẩm',1,1,1,1,1),
		('Đường cách mệnh','Đường cách mệnh là quyển sách được tác giả viết vào năm 1930..',1,2,2,2,1);
      
SELECT * FROM books;

CREATE TABLE prices(
id INT(11) AUTO_INCREMENT NOT NULL,
origin_price DECIMAL(12,2),
promotion_price DECIMAL(12,2),
final_price DECIMAL(12,2),
PRIMARY KEY (id),
book_id INT,
is_thumbnail tinyint NOT NULL,
FOREIGN KEY(book_id) REFERENCES books(id)
)CHARACTER SET = utf8 , COLLATE = utf8_unicode_ci ;

INSERT INTO prices(origin_price, promotion_price, final_price,book_id,is_thumbnail)
VALUES 	('20.000','18.000', '18.000',1,1),
        ('18.000','14000','14000',2,1);
SELECT * FROM prices;

CREATE TABLE pictures(
id INT(11) AUTO_INCREMENT NOT NULL,
name VARCHAR(255) NOT NULL,
book_id INT ,
date_insert datetime,
PRIMARY KEY(id),
is_thumbnail tinyint NOT NULL,
FOREIGN KEY(book_id) REFERENCES books(id)
)CHARACTER SET = utf8 , COLLATE = utf8_unicode_ci ;
        
CREATE TABLE quantities(
id INT(11) AUTO_INCREMENT NOT NULL,
book_id INT(11) NOT NULL, 
origin INT(11) NOT NULL,
remain INT(11) NOT NULL,
product_date date not null,
PRIMARY KEY(id),
FOREIGN KEY(book_id) REFERENCES books(id)
)CHARACTER SET = utf8 , COLLATE = utf8_unicode_ci ;



CREATE TABLE users(
id INT(11) AUTO_INCREMENT NOT NULL,
name VARCHAR(255) NOT NULL,
password VARCHAR(255) NOT NULL,
address VARCHAR(255) NOT NULL,
email VARCHAR(255) NOT NULL,
phone VARCHAR(255) NOT NULL,
is_admin TINYINT,
is_thumbnail tinyint NOT NULL,
PRIMARY KEY(id)
)CHARACTER SET = utf8 , COLLATE = utf8_unicode_ci ;


insert into users(name,password,address,email,phone,is_admin,is_thumbnail)
value ('admin',md5('admin0104'),'quang nam','admin@gmail.com','0123456789',1,1),
	  ('vy',md5('phanvy0104'),'quang nam','phanthaovy.com@gmail.com','0123456789',1,0);



CREATE TABLE orders(
id INT(11) AUTO_INCREMENT NOT NULL,
user_id INT(11) NOT NULL,
order_date DATE NOT NULL,
shipper_date DATE NOT NULL,
email VARCHAR(255) NOT NULL,
name VARCHAR(255) NOT NULL,
address VARCHAR(255) NOT NULL,
phone VARCHAR(255) NOT NULL,
PRIMARY KEY(id),
FOREIGN KEY(user_id) REFERENCES users(id)
)CHARACTER SET = utf8 , COLLATE = utf8_unicode_ci ;

CREATE TABLE order_products(
id INT(11) AUTO_INCREMENT NOT NULL,
order_id INT(11),
book_id INT(11),
quantity INT(11) NOT NULL,
price DECIMAL NOT categoriesNULL, 
PRIMARY KEY(id),
FOREIGN KEY(order_id) REFERENCES orders(id),
FOREIGN KEY(book_id) REFERENCES books(id)
)CHARACTER SET = utf8 , COLLATE = utf8_unicode_ci ;



CREATE TABLE comments(
id INT PRIMARY KEY AUTO_INCREMENT,
user_id INT ,
content VARCHAR(255),
date_comment DATETIME,
FOREIGN KEY(user_id) REFERENCES users(id)
)CHARACTER SET = utf8 , COLLATE = utf8_unicode_ci ;

