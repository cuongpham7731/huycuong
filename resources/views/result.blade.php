 <div >
     <p><span>Kết quả tìm kiếm cho "{{$keyword}}"</span></p>
 </div>
@if(count($products) > 0)
@foreach($products as $product )
<div class="tg-minicarproduct">
    <figure>
        <div class="tg-frontcover"><img style="width: 50px !important;height: 50px !important;" src="{{$product->pictures->last()["url"]?asset($product->pictures->last()["url"]): 'http://placehold.it/400x400'}}" alt="image description"></div>
    </figure>
    <div class="tg-minicarproductdata">
        <li><a href="{{ url('product_detail/'.$product->id) }}">{{ $product->name }} </a></li>
    </div>
</div>

@endforeach
@elseif(count($authors)>0)
    @foreach($authors as $author )
    <div class="tg-minicarproduct">
        <figure>
            <div class="tg-frontcover"><img style="height:50px !important;width: 50px;"src="{{$author->image?asset($author->image): 'http://placehold.it/400x400'}}" alt="image description"></div>
        </figure>
        <div class="tg-minicarproductdata">
            <li><a href="#">{{ $author->name }} </a></li>
        </div>
    </div>
    @endforeach
@else
<li>No Results Found</li>
@endif

