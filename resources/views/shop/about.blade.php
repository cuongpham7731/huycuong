@extends('layouts.layout')
@section('content')
<!--************************************
                Inner Banner Start
*************************************-->
<div class="tg-innerbanner tg-haslayout tg-parallax tg-bginnerbanner" data-z-index="-100" data-appear-top-offset="600" data-parallax="scroll" data-image-src="images/parallax/bgparallax-07.jpg">
    <div class="container">
        <div class="row">
            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                <div class="tg-innerbannercontent">
                    <h1>About Us</h1>
                    <ol class="tg-breadcrumb">
                        <li><a href="javascript:void(0);">home</a></li>
                        <li class="tg-active">About Us</li>
                    </ol>
                </div>
            </div>
        </div>
    </div>
</div>
<!--************************************
                Inner Banner End
*************************************-->
<!--************************************
                Main Start
*************************************-->
<main id="tg-main" class="tg-main tg-haslayout">
    <!--************************************
                    About Us Start
    *************************************-->
    <section class="tg-sectionspace tg-haslayout">
        <div class="container">
            <div class="row">
                <div class="tg-aboutus">
                    <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
                        <div class="tg-aboutusshortcode">
                            <div class="tg-sectionhead">
                                <h2><span>Greetings &amp; Welcome</span>Know More About Us</h2>
                            </div>
                            <div class="tg-description">
                                <p>
                                    Our actions in Vietnam
                                    Launched in 2010 in Danang, Passerelles numériques Vietnam (PNV) offers an IT training programs in Software Development & Testing, based on a holistic educative approach including technical and soft skills, and a personal development program.</p>
                                <p>     Before initiating the program, Passerelles numériques conducted a thorough study on socio-economic status of Vietnam. Demographic data revealed that the vast majority of population in remote areas and the Central Highlands suffered from stagnant economy and limited access to higher education.

                                    Danang was considered as a promising hub for IT development with strong support and incentives granted by the Government. Therefore, our graduates would have various job opportunities as well as employment security after graduating.</p>
                            </div>
                            <div class="tg-btns">
                                <a class="tg-btn tg-active" href="javascript:void(0);">Our History</a>
                                <a class="tg-btn" href="javascript:void(0);">Meet Our Team</a>
                            </div>
                        </div>
                    </div>
                    <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
                        <figure>
                            <img src="images/placeholder.jpg" alt="image description">
                            <iframe src="https://www.youtube.com/embed/dCaNfuiaTtE"></iframe>
                        </figure>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <!--************************************
                    About Us End
    *************************************-->
    <!--************************************
                    Call to Action Start
    *************************************-->

    <!--************************************
                    Call to Action End
    *************************************-->
    <!--************************************
                    Success Start
    *************************************-->
    <section class="tg-sectionspace tg-haslayout">
        <div class="container">
            <div class="row">
                <div class="tg-successstory">
                    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                        <div class="tg-sectionhead">
                            <h2><span>Our Pride Moments</span>Journey of Success</h2>
                        </div>
                        <div id="tg-successslider" class="tg-successslider tg-success owl-carousel">
                            <div class="item">
                                <figure>
                                    <img src="{{asset('images/pnv.jpg')}}" style="width: 500px" alt="image description">
                                </figure>
                                <div class="tg-successcontent">
                                    <div class="tg-sectionhead">
                                        <h2><span>June 27, 2017</span>First Step Toward Success</h2>
                                    </div>
                                    <div class="tg-description">
                                        <p>Consectetur adipisicing elit sed do eiusmod tempor incididunt labore toloregna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamcoiars nisiuip commodo consequat aute irure dolor in aprehenderit aveli esseati cillum dolor fugiat nulla pariatur cepteur sint occaecat cupidatat.</p>
                                    </div>
                                </div>
                            </div>
                            <div class="item">
                                <figure>
                                    <img src="images/img-01.jpg" alt="image description">
                                </figure>
                                <div class="tg-successcontent">
                                    <div class="tg-sectionhead">
                                        <h2><span>June 27, 2017</span>First Step Toward Success</h2>
                                    </div>
                                    <div class="tg-description">
                                        <p>Consectetur adipisicing elit sed do eiusmod tempor incididunt labore toloregna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamcoiars nisiuip commodo consequat aute irure dolor in aprehenderit aveli esseati cillum dolor fugiat nulla pariatur cepteur sint occaecat cupidatat.</p>
                                    </div>
                                </div>
                            </div>
                            <div class="item">
                                <figure>
                                    <img src="images/img-01.jpg" alt="image description">
                                </figure>
                                <div class="tg-successcontent">
                                    <div class="tg-sectionhead">
                                        <h2><span>June 27, 2017</span>First Step Toward Success</h2>
                                    </div>
                                    <div class="tg-description">
                                        <p>Consectetur adipisicing elit sed do eiusmod tempor incididunt labore toloregna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamcoiars nisiuip commodo consequat aute irure dolor in aprehenderit aveli esseati cillum dolor fugiat nulla pariatur cepteur sint occaecat cupidatat.</p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!--************************************
                    Success End
    *************************************-->
    <!--************************************
                    Authors Start
    *************************************-->
    <section class="tg-sectionspace tg-haslayout">
        <div class="container">
            <div class="row">
                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                    <div class="tg-sectionhead">
                        <h2><span>Team Behind Book Library</span>Meet Our Professionals</h2>
                    </div>
                </div>
                <div id="tg-teamsslider" class="tg-authors tg-authorsslider tg-teamsmember owl-carousel">
                    <div class="item tg-author tg-member">
                        <figure><a href="javascript:void(0);"><img src="{{asset('images/author/team/thao.JPG')}}" alt="image description"></a></figure>
                        <div class="tg-authorcontent">
                            <h2><a href="javascript:void(0);">Đinh Thị Thảo</a></h2>
                            <span>Team member</span>
                            <ul class="tg-socialicons">
                                <li class="tg-facebook"><a href="javascript:void(0);"><i class="fa fa-facebook"></i></a></li>
                                <li class="tg-twitter"><a href="javascript:void(0);"><i class="fa fa-twitter"></i></a></li>
                                <li class="tg-linkedin"><a href="javascript:void(0);"><i class="fa fa-linkedin"></i></a></li>
                            </ul>
                        </div>
                    </div>
                    <div class="item tg-author tg-member">
                        <figure><a href="javascript:void(0);"><img src="{{asset('images/author/team/vy.JPG')}}" alt="image description"></a></figure>
                        <div class="tg-authorcontent">
                            <h2><a href="javascript:void(0);">Phan Thị Thảo Vy</a></h2>
                            <span>Team member</span>
                            <ul class="tg-socialicons">
                                <li class="tg-facebook"><a href="javascript:void(0);"><i class="fa fa-facebook"></i></a></li>
                                <li class="tg-twitter"><a href="javascript:void(0);"><i class="fa fa-twitter"></i></a></li>
                                <li class="tg-linkedin"><a href="javascript:void(0);"><i class="fa fa-linkedin"></i></a></li>
                            </ul>
                        </div>
                    </div>
                    <div class="item tg-author tg-member">
                        <figure><a href="javascript:void(0);"><img src="{{asset('images/author/team/dong.JPG')}}" alt="image description"></a></figure>
                        <div class="tg-authorcontent">
                            <h2><a href="javascript:void(0);">Nguyễn Thị Đông</a></h2>
                            <span>Team member</span>
                            <ul class="tg-socialicons">
                                <li class="tg-facebook"><a href="javascript:void(0);"><i class="fa fa-facebook"></i></a></li>
                                <li class="tg-twitter"><a href="javascript:void(0);"><i class="fa fa-twitter"></i></a></li>
                                <li class="tg-linkedin"><a href="javascript:void(0);"><i class="fa fa-linkedin"></i></a></li>
                            </ul>
                        </div>
                    </div>
                    <div class="item tg-author tg-member">
                        <figure><a href="javascript:void(0);"><img src="{{asset('images/author/team/cuong.JPG')}}" alt="image description"></a></figure>
                        <div class="tg-authorcontent">
                            <h2><a href="javascript:void(0);">Phạm Huy Cường</a></h2>
                            <span>Team leader</span>
                            <ul class="tg-socialicons">
                                <li class="tg-facebook"><a href="javascript:void(0);"><i class="fa fa-facebook"></i></a></li>
                                <li class="tg-twitter"><a href="javascript:void(0);"><i class="fa fa-twitter"></i></a></li>
                                <li class="tg-linkedin"><a href="javascript:void(0);"><i class="fa fa-linkedin"></i></a></li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!--************************************
                    Authors End
    *************************************-->
</main>
<!--************************************
                Main End
*************************************-->

@stop