
@extends('layouts.layout')
@section('content')
<div class="pg-header">
    <div class="container">
        <br><br>
        <div class="row">
            <div class="col-md-8 col-sm-12 title">
                <h1 >Shopping Cart</h1>
                @if(session()->has('cart'))

                <br>
                <a href="{{url('/delete-cart')}}" class="remove-cart"><h5 class="text-right btn btn-default btn-warning">Clear All</h5></a>
                <br><br>

                @endif
            </div>
            <div class="col-md-4 col-sm-12">
                <div class="b-crumbs pull-right"><a href="#">Home</a> <i class="arrow_carrot-right">/</i> Basic Page</div>
            </div>
        </div>
    </div>
</div>
<div class="pg-body">
    <div class="container">
        <div class="row">
            @if(session()->has('cart'))
            <div class="table-responsive cart_info">
                <table class="table table-condensed">
                    <thead>
                        <tr class="cart_menu">
                            <td class="image">Item</td>
                            <td class="description">Description</td>
                            <td class="price">Price</td>
                            <td class="quantity">Quantity</td>
                            <td class="total">Total</td>
                            <td class="delete"> Delete</td>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach(session()->get('cart')->items as $key => $item)
                        <tr>
                            <td class="cart_product">
                                <a href=""><img class="img-responsive" width="150" src="{{$item["item"]->pictures->last()["url"]}}" alt=""></a>
                            </td>
                            <td class="cart_description">
                                <h4><a href="{{url('/')}}">Colorblock Scuba</a></h4>
                                <p>Size: {{$item["item"]->dimensions}}</p>
                            </td>
                            <td class="cart_price">
                                <p>$59</p>
                            </td>
                            <td class="cart_quantity">
                                <button class="cart_quantity_button">
                                    <a class="cart_quantity_up" href=""> - </a>
                                    <input class="cart_quantity_input" type="text" name="quantity" value="1" autocomplete="off" size="2">
                                    <a class="cart_quantity_down" href=""> + </a>
                                </button>
                            </td>
                            <td class="cart_total">
                                <p class="cart_total_price">$59</p>
                            </td>
                            <td class="cart_delete">
                                <a class="cart_quantity_delete" href=""><i class="fa fa-times"></i></a>
                            </td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
            @else
            <h4>No products in your cart</h4>
            <a class="btn btn-default btn-warning" href="{{url('/')}}">Back to Homepage</a>
            <br><br>
            @endif
        </div>
    </div>
</div> <!-- pg-body -->
@endsection

