
@extends('layouts.layout')
@section('content')

<div id="tg-homeslider" class="tg-homeslider tg-haslayout owl-carousel">
    <div class="item" data-vide-options="position: 0% 50%">
        <div class="container">
            <div class="row">
                <div class="col-xs-12">
                    <div class="tg-slidercontent">
                        <a href="javascript:void(0);"><img src="images/author/gf0.jpg" alt="image description"></a>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="item"  data-vide-options="position: 0% 50%">
        <div class="container">
            <div class="col-xs-12">
                <div class="tg-slidercontent">
                    <a href="javascript:void(0);"><img src="images/author/bia1.jpg" alt="image description"></a>
                </div>
            </div>
        </div>
    </div>
    <div class="item"  data-vide-options="position: 0% 50%">
        <div class="container">
            <div class="col-xs-12">
                <div class="tg-slidercontent">
                    <a href="javascript:void(0);"><img src="images/author/bia2.jpg" alt="image description"></a>
                </div>
            </div>
        </div>
    </div>
</div>
<!--************************************
                Home Slider End
*************************************-->
<!--************************************
                Main Start
*************************************-->
<main id="tg-main" class="tg-main tg-haslayout">
    <!--************************************
                    All Status Start
    *************************************-->
    <section class="tg-haslayout">
        <div class="container">
            <div class="row">
                <div class="tg-allstatus">
                    <div class="col-xs-12 col-sm-4 col-md-4 col-lg-4">
                        <div class="tg-parallax tg-bgbookwehave" data-z-index="2" data-appear-top-offset="600" data-parallax="scroll" data-image-src="images/parallax/bgparallax-01.jpg">
                            <div class="tg-status">
                                <div class="tg-statuscontent">
                                    <span class="tg-statusicon"><i class="icon-book"></i></span>
                                    <h2>Books we have

                                        <span> {{$numberProduct}}</span>

                                    </h2>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-xs-12 col-sm-4 col-md-4 col-lg-4">
                        <div class="tg-parallax tg-bgtotalmembers" data-z-index="2" data-appear-bottom-offset="600" data-parallax="scroll" data-image-src="images/parallax/bgparallax-02.jpg">
                            <div class="tg-status">
                                <div class="tg-statuscontent">
                                    <span class="tg-statusicon"><i class="icon-user"></i></span>
                                    <h2>Total members
                                        @if(count($numberUser))
                                        <span> {{count($numberUser)}}</span>
                                        @endif</h2>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-xs-12 col-sm-4 col-md-4 col-lg-4">
                        <div class="tg-parallax tg-bghappyusers" data-z-index="2" data-appear-top-offset="600" data-parallax="scroll" data-image-src="images/parallax/bgparallax-03.jpg">
                            <div class="tg-status">
                                <div class="tg-statuscontent">
                                    <span class="tg-statusicon"><i class="icon-heart"></i></span>
                                    <h2>Total product have sale<span>{{$numberproductsale}}</span></h2>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!--************************************
                    All Status End
    *************************************-->
    <!--************************************
                    Best Selling Start
    *************************************-->
    <section class="tg-sectionspace tg-haslayout">
        <div class="container">
            <div class="row">
                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                    <div class="tg-sectionhead">
                        <h2><span>People’s Choice</span>Bestselling Books</h2>
                        <a class="tg-btn" href="{{asset('all_product')}}">View All</a>
                    </div>
                </div>
                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                    <div id="tg-bestsellingbooksslider" class="tg-bestsellingbooksslider tg-bestsellingbooks owl-carousel">
                        @if($bestSellProducts != null)
                        @foreach($bestSellProducts as $product)	

                        <div class="item">
                            <div class="tg-postbook">
                                <figure class="tg-featureimg">
                                    <div class="tg-bookimg">
                                        <a  href="{{asset('product_detail/'.$product->first()["id"])}}">  
                                            <div class="tg-frontcover"><img id="imageproduct" src="{{$product->first()->pictures->first()->url?asset($product->first()->pictures->first()->url): 'http://placehold.it/400x400'}}" alt="image description"></div>
                                            <div class="tg-backcover"><img id="imageproduct" src="{{$product->first()->pictures->last()->url?asset($product->first()->pictures->last()->url): 'http://placehold.it/400x400'}}" alt="image description"></div>
                                        </a>      
                                    </div>
                                    <a class="tg-btnaddtowishlist" href="{{asset('product_detail/'.$product->first()["id"])}}">
                                        <i class="icon-heart"></i>
                                        <span>View detail</span>
                                    </a>
                                </figure>
                                <div class="tg-postbookcontent">
                                    <ul class="tg-bookscategories">
                                        <li><a href="javascript:void(0);">Adventure</a></li>
                                        <li><a href="javascript:void(0);">Fun</a></li>
                                    </ul>
                                    <div class="tg-themetagbox"><span class="tg-themetag">sale</span></div>
                                    <div class="tg-booktitle">
                                        <h3><a href="javascript:void(0);">{{str_limit($product->first()["name"],15)}}</a></h3>
                                    </div>
                                    <span class="tg-bookwriter">By: <a href="javascript:void(0);">{{$product->first()["author"]-> name}}</a></span>
                                    <span class="tg-stars"><span></span></span>
                                    <span class="tg-bookprice">
                                        <ins>${{$product->first()["final_price"]-(($product->first()["promotion_price"]*$product->first()["final_price"])/100  )}}</ins>
                                        <del>${{$product->first()["final_price"]}}</del>
                                    </span>
                                    <a class="tg-btn tg-btnstyletwo add-to-cart" value="{{$product->first()["id"]}}" href="javascript:void(0);">
                                        <i class="fa fa-shopping-basket"></i>
                                        <em>Add To Basket</em>
                                    </a>
                                </div>
                            </div>
                        </div>


                        @endforeach
                        @endif            


                    </div>
                </div>

            </div>
        </div>
    </section>
    <!--************************************
                    Best Selling End
    *************************************-->
    <!--************************************
                    Featured Item Start
    *************************************-->

    <!--************************************
                    Featured Item End
    *************************************-->
    <!--************************************
                    New Release Start
    *************************************-->
    <section class="tg-sectionspace tg-haslayout">
        <div class="container">
            <div class="row">
                <div class="tg-newrelease">
                    <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
                        <div class="tg-sectionhead">
                            <h2><span>Taste The New Spice</span>New Release Books</h2>
                        </div>
                        <div class="tg-description">
                            <p>Consectetur adipisicing elit sed do eiusmod tempor incididunt labore toloregna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamcoiars nisiuip commodo consequat aute irure dolor in aprehenderit aveli esseati cillum dolor fugiat nulla pariatur cepteur sint occaecat cupidatat.</p>
                        </div>
                        <div class="tg-btns">
                            <a class="tg-btn tg-active" href="javascript:void(0);">View All</a>
                            <a class="tg-btn" href="javascript:void(0);">Read More</a>
                        </div>
                    </div>
                    <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
                        <div class="row">
                            <div class="tg-newreleasebooks">
                                @foreach($newProducts as $product)
                                <div class="col-xs-4 col-sm-4 col-md-6 col-lg-4">
                                    <div class="tg-postbook">
                                        <figure class="tg-featureimg">
                                            <div class="tg-bookimg">
                                                <a  href="{{asset('product_detail/'.$product->id)}}">  
                                                    <div class="tg-frontcover"><img id="imageproduct" src="{{$product->pictures->last()["url"]?asset($product->pictures->last()["url"]): 'http://placehold.it/400x400'}}" alt="image description"></div>
                                                    <div class="tg-backcover"><img id="imageproduct" src="{{$product->pictures->last()["url"]?asset($product->pictures->last()["url"]): 'http://placehold.it/400x400'}}" alt="image description"></div>
                                                </a>
                                            </div>
                                            <a class="tg-btnaddtowishlist add-to-cart" href="javascript:void(0);" value="{{$product->id}}">
                                                <i class="icon-cart"></i>
                                                <span>add to cart</span>
                                            </a>
                                        </figure>
                                        <div class="tg-postbookcontent">
                                            <ul class="tg-bookscategories">
                                                <li><a href="javascript:void(0);">{{$product->type->name}}</a></li>

                                            </ul>
                                            <div class="tg-booktitle">
                                                <h3><a href="{{asset('product_detail/'.$product->id)}}">{{$product->name}}</a></h3>
                                            </div>
                                            <span class="tg-bookwriter">By: <a href="javascript:void(0);">{{$product->author->name}}</a></span>
                                            <span class="tg-stars"><span></span></span>
                                        </div>
                                    </div>
                                </div>
                                @endforeach
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!--************************************
                    New Release End
    *************************************-->
    <!--************************************
                    Collection Count Start
    *************************************-->
    <section class="tg-parallax tg-bgcollectioncount tg-haslayout" data-z-index="-100" data-appear-top-offset="600" data-parallax="scroll" data-image-src="images/parallax/bgparallax-04.jpg">
        <div class="tg-sectionspace tg-collectioncount tg-haslayout">
            <div class="container">
                <div class="row">
                    <div id="tg-collectioncounters" class="tg-collectioncounters">
                        <div class="tg-collectioncounter tg-drama">
                            <div class="tg-collectioncountericon">
                                <i class="icon-bubble"></i>
                            </div>
                            <div class="tg-titlepluscounter">
                                <h2>Drama</h2>
                                <h3 data-from="0" data-to="6179213" data-speed="8000" data-refresh-interval="50">6,179,213</h3>
                            </div>
                        </div>
                        <div class="tg-collectioncounter tg-horror">
                            <div class="tg-collectioncountericon">
                                <i class="icon-heart-pulse"></i>
                            </div>
                            <div class="tg-titlepluscounter">
                                <h2>Horror</h2>
                                <h3 data-from="0" data-to="3121242" data-speed="8000" data-refresh-interval="50">3,121,242</h3>
                            </div>
                        </div>
                        <div class="tg-collectioncounter tg-romance">
                            <div class="tg-collectioncountericon">
                                <i class="icon-heart"></i>
                            </div>
                            <div class="tg-titlepluscounter">
                                <h2>Romance</h2>
                                <h3 data-from="0" data-to="2101012" data-speed="8000" data-refresh-interval="50">2,101,012</h3>
                            </div>
                        </div>
                        <div class="tg-collectioncounter tg-fashion">
                            <div class="tg-collectioncountericon">
                                <i class="icon-star"></i>
                            </div>
                            <div class="tg-titlepluscounter">
                                <h2>Fashion</h2>
                                <h3 data-from="0" data-to="1158245" data-speed="8000" data-refresh-interval="50">1,158,245</h3>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!--************************************
                    Collection Count End
    *************************************-->
    <!--************************************
                    Picked By Author Start
    *************************************-->
    <section class="tg-sectionspace tg-haslayout">
        <div class="container">
            <div class="row">
                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                    <div class="tg-sectionhead">
                        <h2><span>Some Great Books</span>Picked By Authors</h2>
                        <a class="tg-btn" href="javascript:void(0);">View All</a>
                    </div>
                </div>
                <div id="tg-pickedbyauthorslider" class="tg-pickedbyauthor tg-pickedbyauthorslider owl-carousel">

                    @if($randomProducts != null)
                    @foreach($randomProducts as $product)	


                    <div class="item">
                        <div class="tg-postbook">
                            <figure class="tg-featureimg">
                                <div class="tg-bookimg">
                                    <a  href="{{asset('product_detail/'.$product->id)}}">  
                                        <div class="tg-frontcover"><img id="imageproduct" src="{{$product->pictures->last()["url"]?asset($product->pictures->last()["url"]): 'http://placehold.it/400x400'}}" alt="image description"></div>
                                        <div class="tg-backcover"><img id="imageproduct" src="{{$product->pictures->last()["url"]?asset($product->pictures->last()["url"]): 'http://placehold.it/400x400'}}" alt="image description"></div>
                                    </a>
                                </div>
                                <div class="tg-hovercontent">
                                    <div class="tg-description">
                                        <p>{{$product->description}}</p>
                                    </div>
                                    <strong class="tg-bookpage">Book Pages:{{$product->page}}</strong>
                                    <strong class="tg-bookcategory">Category:{{$product->type->name}}</strong>
                                    <strong class="tg-bookprice">Price: {{$product->final_price}}</strong>
                                    <div class="tg-ratingbox"><span class="tg-stars"><span></span></span></div>
                                </div>
                            </figure>
                            <div class="tg-postbookcontent">
                                <div class="tg-booktitle">
                                    <h3><a href="{{asset('product_detail/'.$product->id)}}">{{$product->name}}</a></h3>
                                </div>
                                <span class="tg-bookwriter">By: <a href="javascript:void(0);">{{$product->author->name}}</a></span>
                                <a class="tg-btn tg-btnstyletwo add-to-cart" value="{{$product->id}}" href="javascript:void(0);">
                                    <i class="fa fa-shopping-basket"></i>
                                    <em>Add To Basket</em>
                                </a>
                            </div>
                        </div>
                    </div>
                    @endforeach
                    @endif    
                </div>
            </div>
        </div>
    </section>
    <!--************************************
                    Picked By Author End
    *************************************-->
    <!--************************************
                    Testimonials Start
    *************************************-->
    <section class="tg-parallax tg-bgtestimonials tg-haslayout" data-z-index="-100" data-appear-top-offset="600" data-parallax="scroll" data-image-src="images/parallax/bgparallax-05.jpg">
        <div class="tg-sectionspace tg-haslayout">
            <div class="container">
                <div class="row">
                    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-8 col-lg-push-2">
                        <div id="tg-testimonialsslider" class="tg-testimonialsslider tg-testimonials owl-carousel">
                            @foreach($authors as $author)
                            <div class="item tg-testimonial">
                                <figure><img style="
                                             width: 135px;
                                             height: 135px;
                                             "src="{{$author->image?asset($author->image): 'http://placehold.it/400x400'}}" alt="image description"></figure>
                                <blockquote><q>{{$author->introduce}}</q></blockquote>
                                <div class="tg-testimonialauthor">
                                    <h3>{{$author->name}}</h3>
                                    <span>21,658 Published Books</span>
                                </div>
                            </div>
                            @endforeach
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!--************************************
                    Testimonials End
    *************************************-->
    <!--************************************
                    Authors Start
    *************************************-->
    <section class="tg-sectionspace tg-haslayout">
        <div class="container">
            <div class="row">
                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                    <div class="tg-sectionhead">
                        <h2><span>Something New&Hot</span>Sale Products</h2>
                        <a class="tg-btn" href="javascript:void(0);">View All</a>
                    </div>
                </div>
                <div id="tg-authorsslider" class="tg-authors tg-authorsslider owl-carousel" >
                    @foreach($saleProducts as $product)
                    <div class="item" style="width: 165px; margin-right: 30px;">
                        <div class="tg-postbook" >
                            <figure class="tg-featureimg">
                                <div class="tg-bookimg">
                                    <a  href="{{asset('product_detail/'.$product->id)}}">  
                                        <div class="tg-frontcover"><img id="imageproduct" src="{{$product->pictures->last()["url"]?asset($product->pictures->last()["url"]): 'http://placehold.it/400x400'}}" alt="image description"></div>
                                        <div class="tg-backcover"><img id="imageproduct" src="{{'http://placehold.it/400x400'}}" alt="image description"></div>
                                    </a>      
                                </div>
                                <a class="tg-btnaddtowishlist" href="{{asset('product_detail/'.$product->id)}}">
                                    <i class="icon-heart"></i>
                                    <span>View detail</span>
                                </a>
                            </figure>
                            <div class="tg-postbookcontent">
                                <ul class="tg-bookscategories">
                                    <li><a href="javascript:void(0);">Adventure</a></li>
                                    <li><a href="javascript:void(0);">Fun</a></li>
                                </ul>
                                <div class="tg-themetagbox"><span class="tg-themetag">sale</span></div>
                                <div class="tg-booktitle">
                                    <h3><a href="javascript:void(0);">{{str_limit($product->name,15)}}</a></h3>
                                </div>
                                <span class="tg-bookwriter">By: <a href="javascript:void(0);">{{$product->author-> name}}</a></span>
                                <span class="tg-stars"><span></span></span>
                                <span class="tg-bookprice">
                                    <ins>${{$product->final_price-(($product->promotion_price*$product->final_price)/100  )}}</ins>
                                    <del>${{$product->final_price}}</del>
                                </span>
                                <a class="tg-btn tg-btnstyletwo add-to-cart" value="{{$product->id}}" href="javascript:void(0);">
                                    <i class="fa fa-shopping-basket"></i>
                                    <em>Add To Basket</em>
                                </a>
                            </div>
                        </div>
                    </div>
                    @endforeach
                </div>
            </div>
        </div>
    </section>
    <!--************************************
                    Authors End
    *************************************-->
    <!--************************************
                    Call to Action Start
    *************************************-->
    <section class="tg-parallax tg-bgcalltoaction tg-haslayout" data-z-index="-100" data-appear-top-offset="600" data-parallax="scroll" data-image-src="images/parallax/bgparallax-06.jpg">
        <div class="tg-sectionspace tg-haslayout">
            <div class="container">
                <div class="row">
                    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                        <div class="tg-calltoaction">
                            <ul class="tg-clientservices" style=" margin-left: 129px;">
                                <li class="tg-devlivery">
                                    <span class="tg-clientserviceicon"><i class="icon-rocket"></i></span>
                                    <div class="tg-titlesubtitle">
                                        <h3>Fast Delivery</h3>
                                        <p>Shipping Worldwide</p>
                                    </div>
                                </li>
                                <li class="tg-discount">
                                    <span class="tg-clientserviceicon"><i class="icon-tag"></i></span>
                                    <div class="tg-titlesubtitle">
                                        <h3>Open Discount</h3>
                                        <p>Offering Open Discount</p>
                                    </div>
                                </li>
                                <li class="tg-quality">
                                    <span class="tg-clientserviceicon"><i class="icon-leaf"></i></span>
                                    <div class="tg-titlesubtitle">
                                        <h3>Eyes On Quality</h3>
                                        <p>Publishing Quality Work</p>
                                    </div>
                                </li>
                                <li class="tg-support">
                                    <span class="tg-clientserviceicon"><i class="icon-heart"></i></span>
                                    <div class="tg-titlesubtitle">
                                        <h3>24/7 Support</h3>
                                        <p>Serving Every Moments</p>
                                    </div>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!--************************************
                    Call to Action End
    *************************************-->

</main>


@endsection

