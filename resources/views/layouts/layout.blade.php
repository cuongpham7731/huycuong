<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta name="_token" content="{{ csrf_token() }}">
        <meta name="public_path" content="{{ asset('/') }}">
        <title>Avenger Admin Theme</title>
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
        <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0, user-scalable=no">
        <meta name="apple-mobile-web-app-capable" content="yes">
        <meta name="apple-touch-fullscreen" content="yes">
        <meta name="description" content="Avenger Admin Theme">
        <meta name="author" content="KaijuThemes">
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <title>BootStrap HTML5 CSS3 Theme</title>
        <meta name="description" content="">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link href="http://www.jqueryscript.net/css/jquerysctipttop.css" rel="stylesheet" type="text/css">
        <link rel="apple-touch-icon" href="apple-touch-icon.png">
        <link rel="stylesheet" href="{{asset('css/bootstrap.min.css')}}">
        <link rel="stylesheet" href="{{asset('css/normalize.css')}}">
        <link rel="stylesheet" href="{{asset('css/font-awesome.min.css')}}">
        <link rel="stylesheet" href="{{asset('css/icomoon.css')}}">
        <link rel="stylesheet" href="{{asset('css/jquery-ui.css')}}">
        <link rel="stylesheet" href="{{asset('css/owl.carousel.css')}}">
        <link rel="stylesheet" href="{{asset('css/transitions.css')}}">
        <link rel="stylesheet" href="{{asset('css/main.css')}}">
        <link rel="stylesheet" href="{{asset('css/color.css')}}">
        <link rel="stylesheet" href="{{asset('css/responsive.css')}}">
        <script src="{{asset('js/vendor/jquery-library.js')}}"></script>
        <script src="{{asset('js/vendor/bootstrap.min.js')}}"></script>
        <script src="{{asset('js/vendor/modernizr-2.8.3-respond-1.4.2.min.js')}}"></script>
        <script src="{{asset('js/owl.carousel.min.js')}}"></script>       
        <script src="{{asset('js/comment.js')}}"></script>      
        <script src="{{asset('js/filter.js')}}"></script>  
        <script src="{{asset('js/jquery.vide.min.js')}}"></script>
        <script src="{{asset('js/countdown.js')}}"></script>
        <script src="{{asset('js/jquery-ui.js')}}"></script>
        <script src="{{asset('js/parallax.js')}}"></script>  
        <script src="{{asset('js/countTo.js')}}"></script>
        <script src="{{asset('js/appear.js')}}"></script>
        <script src="{{asset('js/diffForHumans.js')}}"></script>
        <script src="{{asset('js/cartAjax.js')}}"></script>
       
        <script src="{{asset('js/main.js')}}"></script>
    </head>
    <body class="tg-home tg-homeone">
        <!--[if lt IE 8]>
                <p class="browserupgrade">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p>
        <![endif]-->
        <!--************************************
                        Wrapper Start
        *************************************-->
        <div id="tg-wrapper" class="tg-wrapper tg-haslayout">
            <!--************************************
                            Header Start
            *************************************-->
            <header id="tg-header" class="tg-header tg-haslayout">
                <div class="tg-topbar">
                    <div class="container">
                        <div class="row">
                            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                <ul class="tg-addnav">
                                    <li>
                                        <a href="{{url('contact-us')}}">
                                            <i class="icon-envelope"></i>
                                            <em>Contact</em>
                                        </a>
                                    </li>
                                    <li>
                                        <a href="javascript:void(0);">
                                            <i class="icon-question-circle"></i>
                                            <em>Help</em>
                                        </a>
                                    </li>
                                </ul>
                                <div class="tg-userlogin">
                                    <figure>
                                        <a href="javascript:void(0);">
                                            <img style="width:30px;height: 30px;margin-top: 10px" 
                                                 src="{{isset(Auth::user()->image)?asset(Auth::user()->image): "http://localhost/laravel-project/public/images/users/img-01.jpg"}}" alt="image description"></a>
                                    </figure>
                                    <ul class="nav navbar-nav navbar-right">
                                        <!-- Authentication Links -->
                                        @guest
                                        <li><a href="{{ route('login') }}">Login</a></li>
                                        <li><a href="{{ route('register') }}">Register</a></li>
                                        @else
                                        <li class="dropdown">
                                            <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false" aria-haspopup="true">
                                                {{ Auth::user()->name }}<i class="fa fa-caret-down"></i>
                                            </a>

                                            <ul class="dropdown-menu">
                                                <li style="list-style: none !important"><a href="{{url('user').'/'.Auth::user()->id}}" >My Account</a></li>
                                                <li style="list-style: none !important">
                                                    <a href="{{ route('logout') }}"
                                                       onclick="event.preventDefault();
    document.getElementById('logout-form').submit();">Logout
                                                    </a>

                                                    <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                                        {{ csrf_field() }}
                                                    </form>
                                                </li>
                                            </ul>
                                        </li>
                                        @endguest
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="tg-middlecontainer">
                    <div class="container">
                        <div class="row">
                            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                <strong class="tg-logo"><a href="{{url("/")}}"><img src="{{asset('images/logo.png')}}" alt="Book Store"></a></strong>
                                <div class="tg-wishlistandcart">
                                    <div class="dropdown tg-themedropdown tg-wishlistdropdown">
                                        <a href="javascript:void(0);" id="tg-wishlisst" class="tg-btnthemedropdown" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                            <span class="tg-themebadge cart-product">0</span>
                                            <i class="icon-heart"></i>
                                            <span>Wishlist</span>
                                        </a>
                                        <div class="dropdown-menu tg-themedropdownmenu" aria-labelledby="tg-wishlisst">
                                            <div class="tg-description"><p>No products were added to the wishlist!</p></div>
                                        </div>
                                    </div>
                                    <div class="dropdown tg-themedropdown tg-minicartdropdown">
                                        <a href="javascript:void(0);" id="tg-minicart" class="tg-btnthemedropdown" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                            <span class="tg-themebadge cart-total-quantity">{{session()->has('cart') && session('cart') != null ? session()->get('cart')->totalQuantity : 0}}</span>
                                            <i class="icon-cart"></i>
                                            <span>$<strong class="cart-total-price">{{session()->has('cart') && session('cart') != null ? session()->get('cart')->totalPrice : 0}}</strong></span>
                                        </a>
                                        <div class="dropdown-menu tg-themedropdownmenu" aria-labelledby="tg-minicart">
                                            <div class="tg-minicartbody">
                                                @if(session()->has('cart') && session('cart') != null)
                                                <?php $count = 0 ?>
                                                @foreach(session()->get('cart')->items as $key => $item)
                                                @if($count < 4)
                                                <div class="tg-minicarproduct">
                                                    <figure>
                                                        <img class="img-responsive" width="80" src="{{$item["item"]["pictures"]->first()["url"]}}" alt="image description">

                                                    </figure>
                                                    <div class="tg-minicarproductdata">
                                                        <h5><a href="javascript:void(0);">{{$item["item"]["name"]}}</a></h5>
                                                        <h6><a href="javascript:void(0);">${{$item["price"]}}</a>      x{{$item["quantity"]}}</h6>
                                                    </div>
                                                </div>
                                                <?php $count ++ ?>
                                                @else
                                                <p style="text-align: center"><a href="{{url('/shopping-cart')}}">View more</a></p>
                                                @break
                                                @endif
                                                @endforeach
                                                @else
                                                <div class="dropdown-menu tg-themedropdownmenu" aria-labelledby="tg-wishlisst">
                                                    <div class="tg-description"><p>No products were added to the wishlist!</p></div>
                                                </div>
                                                @endif
                                            </div>
                                            <div class="tg-minicartfoot">
                                                @if(session()->has('cart') && session('cart') != null)

                                                <a class="tg-btnemptycart" href="{{url('/delete-cart')}}" id="clear-all-cart">
                                                    <i class="fa fa-trash-o"></i>
                                                    <span>Clear Your Cart</span>
                                                </a>
                                                <span class="tg-subtotal">Subtotal: <strong class="cart-total-price">{{session()->has('cart') ? session()->get('cart')->totalPrice : 0}}</strong> <trong>$</trong></span>
                                                <div class="tg-btns">
                                                    <a class="tg-btn tg-active cart-btn-show" href="{{url('/shopping-cart')}}">View Cart</a>
                                                </div>

                                                @endif
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="tg-searchbox">
                                    <form class="tg-formtheme tg-formsearch" action="{{ url('/show') }}" method="GET">
                                        <fieldset>
                                            <input type="text" name="search" id="search" class="typeahead form-control" placeholder="Search by title, author, keyword, ISBN...">
                                            <button type="submit"><i class="icon-magnifier"></i></button>
                                        </fieldset>
                                        <a href="javascript:void(0);">+  Advanced Search</a>
                                    </form>
                                    <div id="result" class="panel panel-default" style="width:400px; position:absolute; right:300px; top:48px; z-index:99999999; display:none">
                                        <ul style="margin-top:10px; list-style-type:none;" id="memList">
                                        </ul>
                                    </div>
                                </div>

                            </div>
                        </div>

                    </div>

                </div>
                <div class="tg-navigationarea">
                    <div class="container">
                        <div class="row">
                            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">

                                <nav id="tg-nav" class="tg-nav">
                                    <div class="navbar-header">
                                        <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#tg-navigation" aria-expanded="false">
                                            <span class="sr-only">Toggle navigation</span>
                                            <span class="icon-bar"></span>
                                            <span class="icon-bar"></span>
                                            <span class="icon-bar"></span>
                                        </button>
                                    </div>

                                    <div id="tg-navigation" class="collapse navbar-collapse tg-navigation">
                                        <ul>
                                            <li class="menu-item-has-children">
                                            <li>
                                                <a href="{{asset('/')}}">
                                                    <span class="glyphicon glyphicon-home"></span>
                                                </a>
                                            </li>
                                            </li>
                                            <li class="menu-item">
                                                <a href="{{url('/all_product')}}">All Products</a>
                                            </li>
                                            <li class="menu-item-has-children">
                                                <a href="javascript:void(0);">Authors</a>
                                                <ul class="sub-menu">             
                                                    <li>
                                                        @if(isset($authors))

                                                        @foreach($authors as $auth)

                                                        <a href="{{asset('author_detail/'.$auth->id)}}">{{isset($auth->name) ? $auth->name : ""}}</a>



                                                        @endforeach
                                                        @endif
                                                    </li>
                                                </ul>
                                            </li>
                                            <li class="menu-item-has-children">
                                                <a href="javascript:void(0);">Types</a>
                                                <ul class="sub-menu">             
                                                    <li>
                                                        @if(isset($types))
                                                        @foreach($types as $type)

                                                        <a href="{{asset('type_detail/'.$type->id)}}">{{isset($type->name) ? $type->name: ""}}</a>

                                                        @endforeach
                                                        @endif
                                                    </li>
                                                </ul>
                                            </li>


                                            <li class="menu-item-has-children">
                                                <a href="javascript:void(0);">Languages</a>
                                                <ul class="sub-menu">             
                                                    <li>
                                                        @if(isset($languages))
                                                        @foreach($languages as $language)

                                                        <a href="{{asset('language_detail/'.$language->id)}}">{{$language->name}}</a>

                                                        @endforeach
                                                        @endif
                                                    </li>
                                                </ul>
                                            </li>
                                            <li class="menu-item-has-children">
                                                <a href="javascript:void(0);">Publishers</a>
                                                <ul class="sub-menu">             
                                                    <li>
                                                        @if(isset($publishers))
                                                        @foreach($publishers as $publisher)

                                                        <a href="{{asset('publisher_detail/'.$publisher->id)}}">{{$publisher->name}}</a>

                                                        @endforeach
                                                        @endif
                                                    </li>
                                                </ul>
                                            </li>


                                            <li class="menu-item">
                                                <a href="{{url('about-us')}}">About us</a>

                                            </li>

                                            <ul class="sub-menu">
                                                <li class="menu-item-has-children">
                                                    <a href="aboutus.html">Products</a>
                                                    <ul class="sub-menu">
                                                        <li><a href="products.html">Products</a></li>
                                                        <li><a href="productdetail.html">Product Detail</a></li>
                                                    </ul>
                                                </li>
                                                <li><a href="aboutus.html">About Us</a></li>
                                                <li><a href="404error.html">404 Error</a></li>
                                                <li><a href="comingsoon.html">Coming Soon</a></li>
                                            </ul>
                                            </li>
                                        </ul>
                                    </div>
                                </nav>
                            </div>
                        </div>
                    </div>
                </div>
            </header>
            <!--************************************
                            Header End
            *************************************-->
            <!--************************************
        
               *************************************-->



            @yield('content')




            <footer id="tg-footer"class="tg-footer tg-haslayout">
                <div class="tg-footerarea">
                    <div class="container">
                        <div class="row">

                            <div class="tg-threecolumns">
                                <div class="col-xs-12 col-sm-6 col-md-4 col-lg-4">
                                    <div class="tg-footercol">
                                        <strong class="tg-logo"><a href="javascript:void(0);"><img src="{{asset('images/flogo.png')}}" alt="image description"></a></strong>
                                        <ul class="tg-contactinfo">
                                            <li>
                                                <i class="icon-apartment"></i>
                                                <address>99 To Hien Thanh Street,Son Tra District, Da Nang City</address>
                                            </li>
                                            <li>
                                                <i class="icon-phone-handset"></i>
                                                <span>
                                                    <em>+84 159 987 365</em>
                                                    <em>0510 3737 703</em>
                                                </span>
                                            </li>
                                            <li>
                                                <i class="icon-clock"></i>
                                                <span>Serving 7 Days A Week From 8am - 8pm</span>
                                            </li>
                                            <li>
                                                <i class="icon-envelope"></i>
                                                <span>
                                                    <em><a href="#">support@domain.com</a></em>
                                                    <em><a href="#">info@domain.com</a></em>
                                                </span>
                                            </li>
                                        </ul>
                                        <ul class="tg-socialicons">
                                            <li class="tg-facebook"><a href="https://www.facebook.com/pages/Passerelles-Num%C3%A9riques-Vietnam/555945917794307"><i class="fa fa-facebook"></i></a></li>
                                            <li class="tg-linkedin"><a href="https://www.linkedin.com/company/passerellesnum-riques/"><i class="fa fa-linkedin"></i></a></li>
                                            <li class="tg-googleplus"><a href="https://www.passerellesnumeriques.org/en/our-actions/vietnam/"><i class="fa fa-google-plus"></i></a></li>

                                            <li class="tg-twitter"><a href="javascript:void(0);"><i class="fa fa-twitter"></i></a></li>

                                        </ul>
                                    </div>
                                </div>
                                <div class="col-xs-12 col-sm-6 col-md-4 col-lg-4">

                                    <div id="map" style="width:350px;height:450px;">
                                        <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3834.110336097462!2d108.24146331480524!3d16.059763193957487!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x3142177f2ced6d8b%3A0xeac35f2960ca74a4!2zOTkgVMO0IEhp4bq_biBUaMOgbmgsIFBoxrDhu5tjIE3hu7ksIFPGoW4gVHLDoCwgxJDDoCBO4bq1bmcsIFZpZXRuYW0!5e0!3m2!1sen!2s!4v1522207695678" width="600" height="450" frameborder="0" style="border:0" allowfullscreen></iframe>
                                    </div> 
                                </div>

                                <div class="col-xs-12 col-sm-12 col-md-4 col-lg-4">
                                    <div class="tg-footercol tg-widget tg-widgettopsellingauthors">
                                        <div class="tg-widgettitle">
                                            <h3>Top Selling Authors</h3>
                                        </div>
                                        <div class="tg-widgetcontent">
                                            <ul>
                                                <li>
                                                    <figure><a href="javascript:void(0);"><img src="{{asset('images/author/imag-09.jpg')}}" alt="image description"></a></figure>
                                                    <div class="tg-authornamebooks">
                                                        <h4><a href="javascript:void(0);">Jude Morphew</a></h4>
                                                        <p>21,658 Published Books</p>
                                                    </div>
                                                </li>
                                                <li>
                                                    <figure><a href="javascript:void(0);"><img src="{{asset('images/author/imag-10.jpg')}}" alt="image description"></a></figure>
                                                    <div class="tg-authornamebooks">
                                                        <h4><a href="javascript:void(0);">Shaun Humes</a></h4>
                                                        <p>20,257 Published Books</p>
                                                    </div>
                                                </li>
                                                <li>
                                                    <figure><a href="javascript:void(0);"><img src="{{asset('images/author/imag-11.jpg')}}" alt="image description"></a></figure>
                                                    <div class="tg-authornamebooks">
                                                        <h4><a href="javascript:void(0);">Kathrine Culbertson</a></h4>
                                                        <p>15,686 Published Books</p>
                                                    </div>
                                                </li>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="tg-footerbar">
                    <a id="tg-btnbacktotop" class="tg-btnbacktotop" href="javascript:void(0);"><i class="icon-chevron-up"></i></a>
                    <div class="container">
                        <div class="row">
                            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                <span class="tg-paymenttype"><img src="{{asset('images/paymenticon.png')}}" alt="image description"></span>
                                <span class="tg-copyright">2018 Made by Cuong_Vy_Thao_Dong</span>
                            </div>
                        </div>
                    </div>
                </div>
            </footer>
            <!--************************************
                            Footer End
            *************************************-->
        </div>
        <!--************************************
                        Wrapper End
        *************************************-->


    </body>

    <script type="text/javascript">

        $(document).ready(function () {
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });
            $('#search').keyup(function () {
                var search = $('#search').val();
                if (search == "") {
                    $("#memList").html("");
                    $('#result').hide();
                } else {
                    $.get("{{ URL::to('search') }}", {search: search}, function (data) {
                        $('#memList').empty().html(data);
                        $('#result').show();
                    })
                }
            });
        });
    </script>
<!--    <script>
        // DOM Ready
        $(function () {

            var $el, $ps, $up, totalHeight;

            $(".sidebar-box .button").click(function () {
                totalHeight = 0

                $el = $(this);
                $p = $el.parent();
                $up = $p.parent();
                $ps = $up.find("p:not('.read-more')");

                // measure how tall inside should be by adding together heights of all inside paragraphs (except read-more paragraph)
                $ps.each(function () {
                    totalHeight += $(this).outerHeight();
                    // FAIL totalHeight += $(this).css("margin-bottom");
                });

                $up
                        .css({
                            // Set height to prevent instant jumpdown when max height is removed
                            "height": $up.height(),
                            "max-height": 9999
                        })
                        .animate({
                            "height": totalHeight
                        });

                // fade out read-more
                $p.fadeOut();

                // prevent jump-down
                return false;

            });

        });
    </script>-->
    <!-- Mirrored from exprostudio.com/html/book_library/index.html by HTTrack Website Copier/3.x [XR&CO'2014], Tue, 20 Mar 2018 01:32:34 GMT -->
</html>