
@extends('layouts.layout')
@section('content')

<div class="tg-innerbanner tg-haslayout tg-parallax tg-bginnerbanner" data-z-index="-100" data-appear-top-offset="600" data-parallax="scroll" data-image-src="{{asset('images/parallax/bgparallax-07.jpg')}}">
    <div class="container">
        <div class="row">
            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                <div class="tg-innerbannercontent">
                    <h1>All Products</h1>
                    <ol class="tg-breadcrumb">
                        <li><a href="{{asset('home')}}">home</a></li>
                        <li><a href="">Products</a></li>
                    </ol>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="container">
<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
    
                        <div class="row">
                            <div class="tg-newreleasebooks">
                                 @if($allTypes != null)
                                    @foreach($allTypes as $product)
                                <div class="col-xs-3 col-sm-3 col-md-3 col-lg-3">
                                    <div class="tg-postbook">
                                        <figure class="tg-featureimg">
                                            <div class="tg-bookimg">
                                                <a  href="{{asset('type_detail/'.$product->id)}}">  
                                                    <div class="tg-frontcover"><img id="imageproduct" src="{{$product->pictures->last()["url"]?asset($product->pictures->last()["url"]): 'http://placehold.it/400x400'}}" alt="image description"></div>
                                                    <div class="tg-backcover"><img id="imageproduct" src="{{$product->pictures->last()["url"]?asset($product->pictures->last()["url"]): 'http://placehold.it/400x400'}}" alt="image description"></div>
                                                </a>
                                            </div>
                                            <a class="tg-btnaddtowishlist add-to-cart" href="{{asset('type_detail/'.$product->id)}}">
                                                <i class="icon-cart"></i>
                                                <span>View detail</span>
                                            </a>
                                        </figure>
                                        <div class="tg-postbookcontent">
                                            <ul class="tg-bookscategories">
                                                <li><a href="javascript:void(0);">{{$product->type->name}}</a></li>

                                            </ul>
                                            <div class="tg-booktitle">
                                                <h3><a href="{{asset('language_detail/'.$product->id)}}">{{$product->name}}</a></h3>
                                            </div>
                                            <span class="tg-bookwriter">By: <a href="javascript:void(0);">{{$product->author->name}}</a></span>
                                            <span class="tg-stars"><span></span></span>
                                        </div>
                                    </div>
                                </div>
                                @endforeach
                        @else
                        <h3>Sản phẩm không tồn tại</h3>
                        @endif    
                            </div>
                        </div>
                    </div>
    </div>
@endsection

