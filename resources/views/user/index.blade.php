@extends('layouts.user')
@section('content')
@if (session('alert'))
<div class="alert alert-success">
    <?php
    echo session('alert');
    ?>
</div>
@endif
<h1>My Account</h1>
<div class="row">
    @include('includes.form_error')
</div>
<div class="container">
    <div class="row">
        <div class=" form-group ">
            <label for="name">UserName:</label>
            <input type="text" id="name" name="name" class="form-control" value="{{Auth::user()->name}}" disabled>
        </div>  
    
        <div class=" form-group ">
            <label for="image">Image:</label>
            <img src="{{$users->image?asset($users->image): 'http://placehold.it/400x400'}}" width="30" alt="" class="img-responsive img-rounded">
        </div>
        <div class=" form-group">
            <label for="address">Address:</label>
            <input type="text" id="address" name="address" class="form-control" value="{{ Auth::user()->address}}"  disabled>

        </div>   
        <div class=" form-group ">
            <label for="phone">Phone:</label>
            <input type="number" id="phone" name="phone" class="form-control" value="{{ Auth::user()->phone}}" disabled>
        </div>   
        <div class="  form-group ">
            <label for="email">Email:</label>
            <input type="text" id="email" name="email" class="form-control" value="{{Auth::user()->email}}" disabled>
        </div>

        <div class="form-group">
            <button class="btn btn-default btn-success"><a href="{{url('user/edit')."/".Auth::user()->id}}">Edit</a></button><td>
        </div>
    </div>    
</div>

@stop