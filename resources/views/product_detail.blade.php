
@extends('layouts.layout')
@section('content')


<!--************************************
                Header End
*************************************-->
<!--************************************
                Inner Banner Start
*************************************-->
<div class="tg-innerbanner tg-haslayout tg-parallax tg-bginnerbanner" data-z-index="-100" data-appear-top-offset="600" data-parallax="scroll" data-image-src="{{asset('images/parallax/bgparallax-07.jpg')}}">
    <div class="container">
        <div class="row">
            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                <div class="tg-innerbannercontent">
                    <h1>All Products</h1>
                    <ol class="tg-breadcrumb">
                        <li><a href="{{asset('home')}}">home</a></li>
                        <li><a href="">Products</a></li>
                        <li class="tg-active">Product Title Here</li>
                    </ol>
                </div>
            </div>
        </div>
    </div>
</div>


<!--************************************
                Inner Banner End
*************************************-->
<!--************************************
                Main Start
*************************************-->
<main id="tg-main" class="tg-main tg-haslayout">
    <!--************************************
                    News Grid Start
    *************************************-->
    <div class="tg-sectionspace tg-haslayout">
        <div class="container">
            <div class="row">
                <div id="tg-twocolumns" class="tg-twocolumns">
                    <div class="col-xs-12 col-sm-4 col-md-4 col-lg-3 pull-left">
                        <aside id="tg-sidebar" class="tg-sidebar">
                            <div class="tg-widget tg-widgetsearch">
                                <form class="tg-formtheme tg-formsearch">
                                    <div class="form-group">
                                        <button type="submit"><i class="icon-magnifier"></i></button>
                                        <input type="search" name="search" class="form-group" placeholder="Search by title, author, key...">
                                    </div>
                                </form>
                            </div>

                            <div class="tg-widget tg-widgettrending">
                                <div class="tg-widgettitle">
                                    <h3>Trending Products</h3>
                                </div>
                                <div class="tg-widgetcontent">
                                    <ul>
                                        @if(count($proByAuthor)>0)
                                        @foreach($proByAuthor as $prod)
                                        <a href="{{asset('product_detail/'.$prod->id)}}">
                                            <li>
                                                <article class="tg-post">
                                                    <figure><a href="{{asset('product_detail/'.$prod->id)}}"><img id="proByAuthor" src="{{$prod->pictures->last()["url"]?asset($prod->pictures->last()["url"]): 'http://placehold.it/400x400'}}" alt="image description"></a></figure>
                                                    <div class="tg-postcontent">
                                                        <div class="tg-posttitle">
                                                            <h3><a href="{{asset('product_detail/'.$prod->id)}}">{{$prod->name}}</a></h3>
                                                        </div>
                                                        <span class="tg-bookwriter">By: <a href="javascript:void(0);">{{$prod->author->name}}</a></span>
                                                    </div>
                                                </article> 
                                            </li>
                                        </a>  
                                        @endforeach
                                        @else {{$proByAuthor}}
                                        @endif
                                    </ul>
                                </div>
                            </div>
                            <div class="tg-widget tg-widgetinstagram">
                                <div class="tg-widgettitle">
                                    <h3>Instagram</h3>
                                </div>
                                <div class="tg-widgetcontent">
                                    <ul>
                                        <li>
                                            <figure>
                                                <img src="{{asset('images/instagram/img-01.jpg')}}" alt="image description">
                                                <figcaption><a href="javascript:void(0);"><i class="icon-heart"></i><em>50,134</em></a></figcaption>
                                            </figure>
                                        </li>
                                        <li>
                                            <figure>
                                                <img src="{{asset('images/instagram/img-02.jpg')}}" alt="image description">
                                                <figcaption><a href="javascript:void(0);"><i class="icon-heart"></i><em>50,134</em></a></figcaption>
                                            </figure>
                                        </li>
                                        <li>
                                            <figure>
                                                <img src="{{asset('images/instagram/img-03.jpg')}}" alt="image description">
                                                <figcaption><a href="javascript:void(0);"><i class="icon-heart"></i><em>50,134</em></a></figcaption>
                                            </figure>
                                        </li>
                                        <li>
                                            <figure>
                                                <img src="{{asset('images/instagram/img-04.jpg')}}" alt="image description">
                                                <figcaption><a href="javascript:void(0);"><i class="icon-heart"></i><em>50,134</em></a></figcaption>
                                            </figure>
                                        </li>
                                        <li>
                                            <figure>
                                                <img src="{{asset('images/instagram/img-05.jpg')}}" alt="image description">
                                                <figcaption><a href="javascript:void(0);"><i class="icon-heart"></i><em>50,134</em></a></figcaption>
                                            </figure>
                                        </li>
                                        <li>
                                            <figure>
                                                <img src="{{asset('images/instagram/img-06.jpg')}}" alt="image description">
                                                <figcaption><a href="javascript:void(0);"><i class="icon-heart"></i><em>50,134</em></a></figcaption>
                                            </figure>
                                        </li>
                                        <li>
                                            <figure>
                                                <img src="{{asset('images/instagram/img-07.jpg')}}" alt="image description">
                                                <figcaption><a href="javascript:void(0);"><i class="icon-heart"></i><em>50,134</em></a></figcaption>
                                            </figure>
                                        </li>
                                        <li>
                                            <figure>
                                                <img src="{{asset('images/instagram/img-08.jpg')}}" alt="image description">
                                                <figcaption><a href="javascript:void(0);"><i class="icon-heart"></i><em>50,134</em></a></figcaption>
                                            </figure>
                                        </li>
                                        <li>
                                            <figure>
                                                <img src="{{asset('images/instagram/img-09.jpg')}}" alt="image description">
                                                <figcaption><a href="javascript:void(0);"><i class="icon-heart"></i><em>50,134</em></a></figcaption>
                                            </figure>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                        </aside>
                    </div>
                    <div class="col-xs-12 col-sm-8 col-md-8 col-lg-9 pull-right">
                        <div id="tg-content" class="tg-content">
                            <div class="tg-productdetail">
                                <div class="row">
                                    <div class="col-xs-12 col-sm-12 col-md-5 col-lg-5">
                                        <div class="tg-postbook">
                                            <span class='zoom' id='ex1'>
                                                <img src="{{$product->pictures->last()["url"]
                         ?asset($product->pictures->last()["url"]): 'http://placehold.it/400x400'}}" width='555' height='320' alt='Daisy on the Ohoopee'/>
                                            </span>
                                            <div id="tg-pickedbyauthorslider" class="tg-pickedbyauthor tg-pickedbyauthorslider owl-carousel">   
                                                @foreach($product->pictures as $pictures)
                                                <div class="item">
                                                    <img width='100px' height='100px'src="{{asset($pictures->url)}}" alt="">
                                                </div>
                                                @endforeach
                                            </div>
                                            <div class="tg-quantityholder">
                                                <em class="minus">-</em>
                                                <input type="text" class="result" value="0" id="quantity1" name="quantity">
                                                <em class="plus">+</em>
                                            </div>
                                            <center> <a class="tg-btn tg-active tg-btn-lg" href="javascript:void(0);">Add To Cart</a>     </center>                                       
                                        </div>
                                    </div>
                                    <div class="col-xs-12 col-sm-12 col-md-7 col-lg-7">
                                        <div class="tg-productcontent">
                                            <ul class="tg-bookscategories">
                                                <li><a href="{{asset('type_detail/'.$product->type->id)}}">{{$product->type->name}}</a></li>
                                            </ul>
                                            <div class="tg-themetagbox"><span class="tg-themetag">sale</span></div>
                                            <div class="tg-booktitle">
                                                <h3>{{$product->name}}</h3>
                                            </div>
                                            <span class="tg-bookwriter">By: <a href="{{asset('author_detail/'.$product->author->id)}}">{{$product->author->name}}</a></span>
                                            <span class="tg-stars"><span></span></span>
                                            <div class="tg-postbookcontent">
                                                <span class="tg-bookprice">
                                                    <ins>${{$product->final_price-(($product->final_price*$product->promotion_price)/100)}}</ins>
                                                    <del>${{$product->final_price}}</del>
                                                </span>
                                                <span class="tg-bookwriter">Sale {{$product->promotion_price}}%</span>
                                                <ul class="tg-delevrystock">
                                                    <li><i class="icon-rocket"></i><span>Free delivery worldwide</span></li>
                                                    <li><i class="icon-checkmark-circle"></i><span>Quantities: {{$product->remain}}</span></li>
                                                    <li><i class="icon-store"></i><span>
                                                            @if($product->remain==0)
                                                            <em>Un stock
                                                            </em>
                                                            @else
                                                            <em>In stock</em>  @endif
                                                        </span></li>
                                                </ul>
                                            </div>
                                            <div class="tg-share">
                                                <span>Share:</span>
                                                <ul class="tg-socialicons">
                                                    <li class="tg-facebook"><a href="https://www.facebook.com/"><i class="fa fa-facebook"></i></a></li>
                                                    <li class="tg-twitter"><a href="https://twitter.com/TypicalGamer?ref_src=twsrc%5Egoogle%7Ctwcamp%5Eserp%7Ctwgr%5Eauthor"><i class="fa fa-twitter"></i></a></li>
                                                    <li class="tg-linkedin"><a href="https://www.linkedin.com/in/thao-dinh-11a41114b/"><i class="fa fa-linkedin"></i></a></li>
                                                    <li class="tg-googleplus"><a href="https://plus.google.com/discover;"><i class="fa fa-google-plus"></i></a></li>             
                                                </ul>
                                            </div>
                                            <div class="tg-description">
                                                <p>{{str_limit($product->description,50)}}...</p><p style="color: darksalmon" data-toggle="collapse" data-target="#demo">More</p>
                                                <div id="demo" class="collapse">
                                                    {{$product->description}}
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="tg-productdescription">
                                        <div class="category-tab shop-details-tab"><!--category-tab-->
                                            <div class="col-sm-12">
                                                <ul class="nav nav-tabs">
                                                    <li class="active"><a href="#product_detail" data-toggle="tab">Detail</a></li>
                                                    <li><a href="#comment" data-toggle="tab">Comment</a></li>
                                                    <li><a href="#infor_author" data-toggle="tab">About author</a></li>
                                                </ul>
                                            </div>
                                            <div class="tab-content">                                                                                                                              
                                                <div class="tab-pane fade active in" id="product_detail" >
                                                    <div class="tg-sectionhead">
                                                        <h2 style=" margin-left: 12px;margin-top: 10px;">Product Details</h2>
                                                    </div>
                                                    <ul class="tg-productinfo">
                                                        <li><span>Name:</span><span>{{$product->name}}</span></li>
                                                        <li><span>Pages:</span><span>528 pages</span></li>
                                                        <li><span>Dimensions:</span><span>{{$product->dimensions}}mm</span></li>
                                                        <li><span>Publication Date:</span><span>{{$product->publication_date}}</span></li>
                                                        <li><span>Publisher:</span><span>{{$product->publisher->name}}</span></li>
                                                        <li><span>Language:</span><span>{{$product->language->name}}</span></li>
                                                    </ul>
                                                </div>
                                                <div class="tab-pane fade" id="infor_author" >
                                                    <div class="tg-aboutauthor">
                                                        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                                            <div class="tg-sectionhead">
                                                                <h2 style="margin-top: 10px;">About Authors</h2>
                                                            </div>
                                                            <div class="tg-authorbox">
                                                                <figure class="tg-authorimg">
                                                                    <img src="{{$product->author->image?asset($product->author->image): 'http://placehold.it/400x400'}}" alt="image description">
                                                                </figure>
                                                                <div class="tg-authorinfo">
                                                                    <div class="tg-authorhead">
                                                                        <div class="tg-leftarea">
                                                                            <div class="tg-authorname">
                                                                                <h2>{{$product->author->name}}</h2>
                                                                            </div>
                                                                        </div>
                                                                        <div class="tg-rightarea">
                                                                            <ul class="tg-socialicons">
                                                                                <li class="tg-facebook"><a href="javascript:void(0);"><i class="fa fa-facebook"></i></a></li>
                                                                                <li class="tg-twitter"><a href="javascript:void(0);"><i class="fa fa-twitter"></i></a></li>
                                                                                <li class="tg-linkedin"><a href="javascript:void(0);"><i class="fa fa-linkedin"></i></a></li>
                                                                                <li class="tg-googleplus"><a href="javascript:void(0);"><i class="fa fa-google-plus"></i></a></li>
                                                                                <li class="tg-rss"><a href="javascript:void(0);"><i class="fa fa-rss"></i></a></li>
                                                                            </ul>
                                                                        </div>
                                                                    </div>
                                                                    <div class="tg-description">
                                                                        <p>{{$product->author->introduce}}</p>
                                                                    </div>
                                                                    <a class="tg-btn tg-active" href="javascript:void(0);">View All Books</a>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="tab-pane fade" id="comment" >
                                                    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                                        <div class="tg-sectionhead">
                                                            <h2 style="margin-top: 10px;">Comment</h2>
                                                        </div>


                                                        <div class="row">
                                                            <div class="col-md-2">
                                                                @if(Auth::user())
                                                                <img class="tg-userbox tg-userimg"  src="{{$user->image?asset($user->image): 'http://placehold.it/400x400'}}" alt="image description">
                                                                @endif
                                                            </div>
                                                            <div class="col-md-10">


                                                                <form id="frmProducts" name="frmProducts" class="form-horizontal" novalidate="">
                                                                    <input type="hidden" id="user_id" name="_token" value="{{Auth::check() ? Auth::user()->id : 0}}"/>
                                                                    <input type="hidden" id="user_image" name="_token" value="{{Auth::check() ? Auth::user()->image : 'http://placehold.it/400x400'}}"/>
                                                                    <input type="hidden" id="comment-product-form-token" name="_token" value="{{ csrf_token() }}"/>
                                                                    <input type="hidden" id="parent_id" name="_token" value="0"/>
                                                                    <div class="form-group error">                        
                                                                        <textarea name="comment_content" class=" form-control" style=" height:100px" id="comment_content" name="comment_content" value="ds"></textarea>      

                                                                </form> </div>
                                                            <div class="modal-footer">
                                                                <button type="button" class="btn btn-primary" id="btn-save" value="{{$product->id}}">Comment</button>

                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="row">
                                                        <div class="newcomment" id="newcomment"></div>   
                                                    </div>
                                                    <div class="row">
                                                        @foreach($comments as $comment)
                                                        @if($comment->parent_id == 0)

                                                        <div class="oldcomment" id="oldcomment" >
                                                            <div class="row">
                                                                <div class="col-md-2">
                                                                    <img  class="tg-userbox tg-userimg" src="{{$comment->user->image?asset($comment->user->image): 'http://placehold.it/400x400'}}" alt="image description">
                                                                </div>
                                                                <div class="col-md-10">
                                                                    <div class="col-md-12 display_comment">
                                                                        <div class="col-md-8">
                                                                            @if(Auth::user())
                                                                            @if($comment->user->name!=Auth::user()->name)
                                                                            <a href="" <i class="fa fa-user"></i>{{$comment->user->name}}</a>
                                                                            @else
                                                                            <a href="" <i class="fa fa-user"></i>You</a>
                                                                            @endif
                                                                            @else
                                                                            <a href="" <i class="fa fa-user"></i>{{$comment->user->name}}</a>
                                                                            @endif
                                                                        </div>
                                                                        <div class="col-md-4">
                                                                            <a href=""><i class="fa fa-clock-o"></i>{{$comment->created_at->diffForhumans()}}</a>
                                                                        </div>
                                                                        <div id="example1">
                                                                            <p>{{$comment->content}}</p>
                                                                        </div>


                                                                    </div>   <br>
                                                                    <details >
                                                                        <summary  >   <p class="tg-btn">Reply</p></summary>
                                                                        <div class="row" id="reply_comment_{{$comment->id}}" class="" >
                                                                            <div class="col-md-2">
                                                                                @if(Auth::user())
                                                                                <img class="tg-userbox tg-userimg"  src="{{$user->image?asset($user->image): 'http://placehold.it/400x400'}}" alt="image description">
                                                                                @endif
                                                                            </div>
                                                                            <div class="col-md-10" >
                                                                                <form id="reply_{{$comment->id}}" name="form_comment" class="form-horizontal" novalidate="">
                                                                                    <input type="hidden" id="user_id" name="_token" value="{{Auth::check() ? Auth::user()->id : 0}}"/>
                                                                                    <input type="hidden" id="user_image_reply" name="_token" value="{{Auth::check() ? Auth::user()->image : 'http://placehold.it/400x400'}}"/>
                                                                                    <input type="hidden" id="parent_id_reply_{{$comment->id}}" name="_token" value="{{$comment->id>0 ? $comment->id : 0}}"/>
                                                                                    <input type="hidden" id="reply-comment-product-form-token" name="_token" value="{{ csrf_token() }}"/>
                                                                                    <input type="hidden" id="product_id_{{$comment->id}}" name="_token" value="{{$product->id }}"/>
                                                                                    <div class="">                        
                                                                                        <textarea name="comment_content_reply"class=" form-control" style="  height:100px" id="comment_content_reply_{{$comment->id}}" name="comment_content" value="ds"></textarea>      
                                                                                </form> </div>
                                                                            <div class="modal-footer">
                                                                                <button class="btn btn-primary reply" data-id="{{$comment ->id}}" value="Reply">Reply</button>
                                                                            </div>
                                                                            <div id="new_reply_{{$comment->id}}"></div>
                                                                        </div>
                                                                </div>
                                                                </details>
                                                                <details >
                                                                    <summary >   <p class="tg-btn" >View reply</p></summary>


                                                                    @if(count($comment->children))
                                                                    @foreach($comment->children as $replyComment)
                                                                    <div class="row" id="view_reply_{{$replyComment->id}}" class="" >

                                                                        <div class="col-md-2">
                                                                            <img style=" width:  50px;height:  50px;" class="tg-userbox tg-userimg" src="{{$replyComment->user->image?asset($replyComment->user->image): 'http://placehold.it/400x400'}}" alt="image description">
                                                                        </div>
                                                                        <div class="col-md-10" style="float: right">
                                                                            <div class="col-md-12 display_comment">
                                                                                <div class="col-md-8">
                                                                                    @if(Auth::user())
                                                                                    @if($replyComment->user->name!=Auth::user()->name)
                                                                                    <a href="" <i class="fa fa-user"></i>{{$replyComment->user->name}}</a>
                                                                                    @else
                                                                                    <a href="" <i class="fa fa-user"></i>You</a>
                                                                                    @endif
                                                                                    @else
                                                                                    <a href="" <i class="fa fa-user"></i>{{$replyComment->user->name}}</a>
                                                                                    @endif
                                                                                </div>
                                                                                <div class="col-md-4">
                                                                                    <a href=""><i class="fa fa-clock-o"></i>{{$replyComment->created_at->diffForhumans()}}</a>
                                                                                </div>
                                                                                <div id="example1">
                                                                                    <p>{{$replyComment->content}}</p>
                                                                                </div>


                                                                            </div>   <br>
                                                                            
                                                                            <details >
                                                                                <summary  >   <p class="tg-btn" >Reply</p></summary>
                                                                                <div class="row">
                                                                                    <div class="col-md-2">
                                                                                        @if(Auth::user())
                                                                                        <img class="tg-userbox tg-userimg"  src="{{$user->image?asset($user->image): 'http://placehold.it/400x400'}}" alt="image description">
                                                                                        @endif
                                                                                    </div>
                                                                                    <div class="col-md-10" id="reply_comment_{{$comment->id}}" class="" >
                                                                                        <form id="reply_{{$replyComment->id}}" name="form_comment" class="form-horizontal" novalidate="">
                                                                                            <input type="hidden" id="user_id" name="_token" value="{{Auth::check() ? Auth::user()->id : 0}}"/>
                                                                                            <input type="hidden" id="user_image_reply" name="_token" value="{{Auth::check() ? Auth::user()->image : 'http://placehold.it/400x400'}}"/>
                                                                                            <input type="hidden" id="parent_id_reply_{{$replyComment->id}}" name="_token" value="{{$replyComment->id>0 ? $replyComment->id : 0}}"/>
                                                                                            <input type="hidden" id="reply-comment-product-form-token" name="_token" value="{{ csrf_token() }}"/>
                                                                                            <input type="hidden" id="product_id_{{$replyComment->id}}" name="_token" value="{{$product->id }}"/>
                                                                                            <div class="">                        
                                                                                                <textarea name="comment_content_reply" class="form-control" id="comment_content_reply_{{$replyComment->id}}" name="comment_content" value="ds"></textarea>      
                                                                                        </form> </div>
                                                                                    <div class="modal-footer">
                                                                                        <button class="btn btn-primary reply" data-id="{{$replyComment ->id}}" value="Reply">Reply</button>
                                                                                    </div>
                                                                                    <div id="new_reply_{{$replyComment->id}}"></div>
                                                                                </div>
                                                                        </div>
                                                                </details>
                                                            </div> </div>@endforeach
                                                        </details>
                                                        @endif
                                                    </div>
                                                </div>

                                            </div>
                                            @endif
                                            @endforeach 

                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>      

                    </div>
                </div>
            </div>
        </div>           
    </div>
</div>
</div>
</div>
</div>
</div>

</div>
</div>
</div>
</div>
<script src="{{asset('js/jquery.zoom.js')}}"></script>

<script type="text/javascript">
$(document).ready(function () {
    $('#ex1').zoom();
    $('#ex2').zoom({on: 'grab'});
    $('#ex3').zoom({on: 'click'});
    $('#ex4').zoom({on: 'toggle'});
});
</script>
</main>
@endsection


