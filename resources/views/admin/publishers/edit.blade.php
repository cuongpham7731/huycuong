@extends('layouts.admin')
@section('content')

    <h1>Publisher</h1>


    <div class="col-sm-6">
        <form action="{{ route('admin.publishers.update', $publisher->id) }}" method="POST" >
            <input type="hidden" name="_token" value="{{ csrf_token() }}"/>
         <input type="hidden" name="_method" value="PUT">
         <div class=" form-group {{ $errors->has('name') ? 'has-error' : '' }}">
                    <label for="username">Name:</label>
                    <input type="text" id="name" name="name" class="form-control" placeholder="Enter publisher name" value="{{ $publisher->name }}">
                    <span class="text-danger">{{ $errors->first('name') }}</span>
            </div>  

        <div class="form-group">
           <input type="submit" class="btn btn-success" value="Update publisher" />
        </div>
        </form>

    </div>


    <div class="col-sm-6">


    </div>


@stop