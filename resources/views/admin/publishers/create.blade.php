
@extends('layouts.admin')

@section('content')


    <h1>Publsher</h1>


    <div class="col-sm-6">
        <form action="{{ route('admin.publishers.store') }}" method="post">
            <input type="hidden" name="_token" value="{{ csrf_token() }}"/>

        

            <div class=" form-group {{ $errors->has('name') ? 'has-error' : '' }}">
                    <label for="name">name:</label>
                    <input type="text" id="name" name="name" class="form-control" placeholder="Enter publisher name" value="{{ old('name') }}">
                    <span class="text-danger">{{ $errors->first('name') }}</span>
            </div>        

           
            <div class="form-group">
                <input type="submit" class="btn btn-success" value="Create publisher" />
            </div>
        </form>
 


    </div>


@endsection
