@extends('layouts.admin')


@section('content')


    <h1>Language</h1>
    <div class="col-sm-6">
        <form action="{{ route('admin.languages.create')}}">
            <div class="form-group">
                <input type="submit" class="btn btn-success" value="Create" />
            </div>
        </form>
    </div>

    <table class="table">
        <thead>
            <tr>
                <th>Id</th>
                <th>Name</th>       
                <th>Created at</th>
                <th>Updated date</th>  
                <th>Edit</th>
                <th>Delete</th>
        </thead>
        <tbody>
            @if($languages)

            @foreach($languages as $language)

            <tr>
                <td>{{$language->id}}</td>
                <td>{{$language->name}}</a></td>
                <td>{{$language->created_at ? $language->created_at->diffForHumans() : 'no date'}}</td>
                <td>{{$language->updated_at ? $language->updated_at->diffForHumans() : 'no date'}}</td>  
                <td>
                    <form action="{{route('admin.languages.edit',$language->id)}}" >
                        <div class="form-group">
                            <input type="submit" class="btn btn-success" value="Edit" />
                        </div>
                    </form>

                </td> 
                <td>  
                    <form action="{{ route('admin.languages.destroy', $language->id) }}" method="POST" >
                        <input type="hidden" name="_method" value="DELETE">
                        <input type="hidden" name="_token" value="{{ csrf_token() }}"/>
                        <div class="form-group">
                            <input type="submit" class="btn btn-danger" value="Delete" />

                        </div>
                    </form>
                </td>
            </tr>
            @endforeach

            @endif

        </tbody>
    </table>
    @if (session('response'))
    <div class="alert {{session('response')["status"] == 'success' ?  'alert-success' : 'alert-danger'}}">
    <strong>{{session('response')["message"]}}</strong> 
  </div>
    @endif
    <div class="row">
        <div class="col-lg-6 col-sm-offset-5">
            {{$languages->render()}}

        </div>
    </div>





@stop