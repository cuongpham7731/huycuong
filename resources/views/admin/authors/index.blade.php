@extends('layouts.admin')


@section('content')


    <h1>Author</h1>
    <div class="col-sm-6">
        <form action="{{ route('admin.authors.create')}}">
            <div class="form-group">
                <input type="submit" class="btn btn-success" value="Create" />
            </div>
        </form>
    </div>

    <table class="table">
        <thead>
            <tr>
                <th>Id</th>
                <th>Name</th>  
                <th>Photo</th>
                <th>Introduce authors</th>
                <th>Created at</th>
                <th>Updated date</th>  
                <th>Edit</th>
                <th>Delete</th>
        </thead>
        <tbody>
            @if($authors)

            @foreach($authors as $author)

            <tr>
                <td>{{$author->id}}</td>
                <td>{{$author->name}}</td>
                <td><img src="{{$author->image ? asset( $author->image): 'http://placehold.it/400x400'}}" width="100" alt="" class="img-responsive img-rounded">
                </td> 
                <td>{{$author->introduce}}
                </td>
                <td>{{$author->created_at ? $author->created_at->diffForHumans() : 'no date'}}</td>
                <td>{{$author->updated_at ? $author->updated_at->diffForHumans() : 'no date'}}</td>  
                <td>
                    <form action="{{route('admin.authors.edit',$author->id)}}" >
                        <div class="form-group">
                            <input type="submit" class="btn btn-success" value="Edit" />
                        </div>
                    </form>

                </td> 
                <td>  
                    <form action="{{ route('admin.authors.destroy',$author->id) }}" method="POST" >
                        <input type="hidden" name="_method" value="DELETE">
                        <input type="hidden" name="_token" value="{{ csrf_token() }}"/>
                        <div class="form-group">
                            <input type="submit" class="btn btn-danger" value="Delete" />

                        </div>
                    </form>
                </td>
            </tr>
            @endforeach

            @endif

        </tbody>
    </table>
    @if (session('response'))
    <div class="alert {{session('response')["status"] == 'success' ?  'alert-success' : 'alert-danger'}}">
        <strong>{{session('response')["message"]}}</strong> 
    </div>
    @endif
    <div class="row">
        <div class="col-lg-6 col-sm-offset-5">
            {{$authors->render()}}

        </div>

    </div>





@stop