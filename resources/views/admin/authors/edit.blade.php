@extends('layouts.admin')
@section('content')

<h1>Author</h1>


<div class="col-sm-6">
    <form action="{{ route('admin.authors.update', $author->id) }}" method="POST" enctype='multipart/form-data'>
        <input type="hidden" name="_token" value="{{ csrf_token() }}"/>
        <input type="hidden" name="_method" value="PUT">

        <div class=" form-group {{ $errors->has('name') ? 'has-error' : '' }}">
            <label for="username">name:</label>
            <input type="text" id="name" name="name" class="form-control" placeholder="Enter author name" value="{{ $author->name }}">
            <span class="text-danger">{{ $errors->first('name') }}</span>
        </div> 
       <div class="  form-group {{ $errors->has('photo_id') ? 'has-error' : '' }}">
            <label for="photo_id">Photo:</label>
            <img src="{{$author->image ? asset( $author->image): 'http://placehold.it/400x400'}}" width="200" alt="" class="img-responsive img-rounded">
            <br>
            <input type="file" id="photo_id" name="photo_id" class="form-control" value="{{ old('photo_id') }}">
            <span class="text-danger">{{ $errors->first('photo_id') }}</span>
        </div>  
        <br>
        <div class=" form-group {{ $errors->has('introduce') ? 'has-error' : '' }}">
            <label for="introduce">Introduce authors:</label>
            <input type="text" id="name" name="introduce" class="form-control" placeholder="Enter author name" value="{{ $author->introduce }}">
            <span class="text-danger">{{ $errors->first('introduce') }}</span>
        </div> 

        <div class="form-group">
            <input type="submit" class="btn btn-success" value="Update Author" />
        </div>
    </form>

</div>


@stop