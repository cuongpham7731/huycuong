
@extends('layouts.admin')

@section('content')


<h1>Author</h1>


<div class="col-sm-6">
    <form action="{{ route('admin.authors.store') }}" method="post" enctype='multipart/form-data' >
        <input type="hidden" name="_token" value="{{ csrf_token() }}"/>

        <div class=" form-group {{ $errors->has('name') ? 'has-error' : '' }}">
            <label for="name">name:</label>
            <input type="text" id="name" name="name" class="form-control" placeholder="Enter author name" value="{{ old('name') }}">
            <span class="text-danger">{{ $errors->first('name') }}</span>
        </div>   
        
        <div class="  form-group {{ $errors->has('photo_id') ? 'has-error' : '' }}">
            <label for="photo_id">Photo:</label>
            <input type="file" id="photo_id" name="photo_id" class="form-control" value="{{ old('photo_id') }}">
            <span class="text-danger">{{ $errors->first('photo_id') }}</span>
        </div> 
        
        <div class=" form-group {{ $errors->has('introduce') ? 'has-error' : '' }}">
            <label for="introduce">Introduce authors:</label>
            <input type="text" id="introduce" name="introduce" class="form-control" placeholder="Enter introduce author" value="{{ old('introduce') }}">
            <span class="text-danger">{{ $errors->first('introduce') }}</span>
        </div> 
        
        <div class="form-group">
            <input type="submit" class="btn btn-success" value="Create author" />
        </div>
    </form>



</div>


@endsection
