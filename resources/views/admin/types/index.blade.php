@extends('layouts.admin')


@section('content')

<div class="container">
<h1>Type</h1>
<div class="col-sm-6">
    <form action="{{ route('admin.types.create')}}">
        <div class="form-group">
            <input type="submit" class="btn btn-success" value="Create" />
        </div>
    </form>
</div>

<table class="table">
    <thead>
        <tr>
            <th>Id</th>
            <th>Name</th>       
            <th>Created at</th>
            <th>Updated date</th>  
            <th>Edit</th>
            <th>Delete</th>
    </thead>
    <tbody>
        @if($types)
        
        @foreach($types as $type)

        <tr>
            <td>{{$type->id}}</td>
            <td>{{$type->name}}</a></td>
            <td>{{$type->created_at ? $type->created_at->diffForHumans() : 'no date'}}</td>
            <td>{{$type->updated_at ? $type->updated_at->diffForHumans() : 'no date'}}</td>  
            <td>
                <form action="{{route('admin.types.edit',$type->id)}}" >
                    <div class="form-group">
                        <input type="submit" class="btn btn-success" value="Edit" />
                    </div>
                </form>

            </td> 
            <td>  
                <form action="{{ route('admin.types.destroy',$type->id) }}" method="POST" >
                    <input type="hidden" name="_method" value="DELETE">
                    <input type="hidden" name="_token" value="{{ csrf_token() }}"/>
                    <div class="form-group">
                        <input type="submit" class="btn btn-danger" value="Delete" />

                    </div>
                </form>
            </td>
        </tr>
        @endforeach

@endif

    </tbody>
</table>
@if (session('response'))
    <div class="alert {{session('response')["status"] == 'success' ?  'alert-success' : 'alert-danger'}}">
    <strong>{{session('response')["message"]}}</strong> 
  </div>
    @endif
<div class="row">
    <div class="col-lg-6 col-sm-offset-5">
        {{$types->render()}}
        
    </div>
    
</div>
</div>
</div>




@stop