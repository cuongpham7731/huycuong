@extends('layouts.admin')
@section('content')
@if (session('alert'))
<div class="alert alert-success">
    <?php
    echo session('alert');
    ?>
</div>
@endif
<div class="container-fuild">

    <h1>Products</h1>

    <button class="btn btn-default btn-success"><a href="{{   url('admin/products/create') }}">Create</a></button><td>
        <table class="table">
            <thead>
                <tr>
                    <th>Id</th>
                    <th>Photo</th>
                    <th>Name</th>
                    <th>Type</th>
                    <th>Author</th>
                    <th>Language</th>
                    <th>Publisher</th>
                    <th>Decription</th>
                    <th>Date publication</th>
                    <th>Pages</th>
                    <th>Dimensions</th> 
                    <th>Origin_price</th>
                    <th>final_price</th>
                    <th>sale</th>
                    <th>origin</th>
                    <th>remain</th>  
                    <th>Created at</th>
                    <th>Update</th>
                    <th>Modify</th>
            </thead>
            <tbody>
                @if(count($products)>0)
                @foreach($products as $product)
                <tr>
                    <td>{{$product->id}}</td>
                    <td><img width="50px" height="50px" class="img-responsive" src="{{$product->pictures->last()["url"]?asset($product->pictures->last()["url"]): 'http://placehold.it/400x400'}}" alt=""></td>
                    <td>{{$product->name}}</td>
                    <td>{{$product->type->name}}</td>
                    <td>{{$product->author->name}}</td>
                    <td>{{$product->language->name}}</td>
                    <td>{{$product->publisher->name}}</td>
                    <td>{{str_limit($product->description, 30)}}</td>   
                    <td>{{$product->publication_date}}</td>
                    <td>{{$product->page}}</td>
                    <td>{{$product->dimensions}}</td>
                    <td>{{$product->origin_price}}</td>
                    <td>{{$product->final_price}}</td>
                    <td>{{$product->promotion_price}}%</td>
                    <td>{{$product->origin}}</td>
                    <td>{{$product->remain}}</td>
                    <td>{{$product->created_at}}</td>
                    <td>{{$product->updated_at}}</td>



                    <td>
                        <button class="btn btn-default btn-success"><a href="{{   url('admin/products/'.$product->id.'/edit') }}">Edit</a></button><td>
                        <form action="{{ route('admin.products.destroy',$product->id) }}" method="POST" >
                            <input type="hidden" name="_method" value="DELETE">
                            <input type="hidden" name="_token" value="{{ csrf_token() }}"/>
                            <div class="form-group">
                                <input type="submit" class="btn btn-danger" value="Delete" />

                            </div>
                        </form>
                    </td>                


                </tr>
                @endforeach
                @endif
            </tbody>
        </table>

</div>
<div class="row">
    <div class="col-lg-6 col-sm-offset-5">
        {{ $products->render() }}
    </div>

</div>




@stop