@extends('layouts.admin')

@section('custom_css')
<link type="text/css" href="{{asset('css/bootstrap-datetimepicker.css')}}" rel="stylesheet" media="screen">
<link type="text/css" href="{{asset('css/bootstrap-datetimepicker.min.css')}}" rel="stylesheet" media="screen">
@endsection

@section('content')

@include('includes.tinyeditor')


<h1 style="text-align: center">Create Product</h1>

<form action="{{ route('admin.products.store') }}" method="post" class="form-horizontal" enctype='multipart/form-data'>
    <input type="hidden" name="_token" value="{{ csrf_token() }}"/>
    <div class="form-group">
        <div class="col-md-1">
            <label>name</label>
        </div>
        <div class="col-md-11">
            <input type="text" class="form-control" name="name"  placeholder="Please enter book name" value="{{ old('name') }}"></input>
            <span class="text-danger">{{ $errors->first('name') }}</span>
        </div>


    </div>
    <div class="form-group">
        <div class="col-md-1">
            <label>Description</label>
        </div>
        <div class="col-md-11">
            <textarea class="form-control" rows="5" id="body" name="description">{{ old('description') }}</textarea>
            <span class="text-danger">{{ $errors->first('description') }}</span>
        </div>


    </div>
    <div class="form-group">
        <div class="col-md-1">
            <label>Type</label>
        </div>
        <div class="col-md-11">

            <select class="form-control" id="type_id" name="type_id" >
                <option value="0"> --Choose type--</option>
                @foreach ($types as $id => $name  )
                <option value="{{$id}}">{{$name}}</option>
                @endforeach 

                <option value="0">--create other type--</option>
            </select>   
            <span class="text-danger">{{ $errors->first('type_id') }}</span>
        </div>


    </div>
    <div class="form-group">
        <div class="col-md-1">
            <label>Author</label>
        </div>
        <div class="col-md-11">
            <select class="form-control" id="author_id" name="author_id" >
                <option value="0"> --Choose author--</option>
                @foreach ($authors as $id => $name  )
                <option value="{{$id}}">{{$name}}</option>
                @endforeach 

                <option value="0">--create other author--</option>
            </select>   
            <span class="text-danger">{{ $errors->first('author_id') }}</span>
        </div>


    </div>
    <div class="form-group">
        <div class="col-md-1">
            <label>Language</label>
        </div>
        <div class="col-md-11">
            <select class="form-control" id="language_id" name="language_id" >
                <option value="0"> --Choose language--</option>
                @foreach ($languages as $id => $name  )
                <option value="{{$id}}">{{$name}}</option>
                @endforeach 

                <option value="0">--create other language--</option>
            </select>   
            <span class="text-danger">{{ $errors->first('language_id') }}</span>
        </div>


    </div>
    <div class="form-group">
        <div class="col-md-1">
            <label>Publisher</label>
        </div>
        <div class="col-md-11">
            <select class="form-control" id="publisher_id" name="publisher_id" >
                <option value="0"> --Choose publisher--</option>
                @foreach ($publishers as $id => $name  )
                <option value="{{$id}}">{{$name}}</option>
                @endforeach 

                <option value="">--create other publisher--</option>
            </select>   
            <span class="text-danger">{{ $errors->first('publisher_id') }}</span>
        </div>


    </div>
    <div class="form-group">
        <div class="col-md-1">
            <label>Page</label>
        </div>
        <div class="col-md-11">
            <input type="number" class="form-control" name="page"  placeholder="Please enter book numbers" value="{{ old('page') }}"></input>
            <span class="text-danger">{{ $errors->first('page') }}</span>
        </div>


    </div>
    <div class="form-group">
        <div class="col-md-1">
            <label>Dimensions</label>
        </div>
        <div class="col-md-11">
            <br>
            <div class="row">
                <div class="col-md-6"
                     <label>Width</label>
                    <input type="number" class="form-control" name="width"  placeholder="Please enter book Width" value="{{ old('width') }}"></input>
                    <span class="text-danger">{{ $errors->first('width') }}</span>
                </div>
                <div class="col-md-6"
                     <label>Heigh</label>
                    <input type="number" class="form-control" name="height"  placeholder="Please enter book Heigh" value="{{ old('height') }}"></input>
                    <span class="text-danger">{{ $errors->first('height') }}</span>
                </div>
            </div>
        </div>


    </div>
    <div class="form-group">
        <div class="col-md-1">
            <label>Publiction date</label>
        </div>
        <div class="col-md-11">

            <fieldset>
                <div class="control-group">

                    <div class="controls input-append date form_date" data-date="" data-date-format="dd MM yyyy" data-link-field="dtp_input2" data-link-format="yyyy-mm-dd">
                        <input size="16" type="text" name="publication_date" value="{{ old('publication_date') }}" readonly>
                        <span class="add-on"><i class="icon-remove"></i></span>
                        <span class="add-on"><i class="icon-th"></i></span>
                        <span class="text-danger">{{ $errors->first('publication_date') }}</span>
                    </div>
                </div>
            </fieldset>

        </div>
    </div>
    <div class="form-group">
        <div class="col-md-1">
            <label>Orgin price</label>
        </div>
        <div class="col-md-11">
            <input type="number" name="origin_price"class="form-control" value="{{ old('origin_price') }}"placeholder="Please enter orgin price">
            <span class="text-danger">{{ $errors->first('origin_price') }}</span>
        </div>
    </div>
    <div class="form-group">
        <div class="col-md-1">
            <label>Final price</label>
        </div>
        <div class="col-md-11">
            <input type="number" name="final_price" class="form-control" placeholder="Please enter final price" value="{{ old('final_price') }}">
            <span class="text-danger">{{ $errors->first('final_price') }}</span>
        </div>


    </div>
    <div class="form-group">
        <div class="col-md-1">
            <label>sale price</label>
        </div>
        <div class="col-md-11">
            <input type="number" name="promotion_price" class="form-control" placeholder="Please enter final price" value="{{ old('promotion_price') }}"><span>%</span>
            <span class="text-danger">{{ $errors->first('promotion_price') }}</span>
        </div>


    </div>
    <div class="form-group">
        <div class="col-md-1">
            <label>Quantity origin</label>
        </div>
        <div class="col-md-11">
            <input type="number" name="origin" class="form-control" placeholder="Please enter quanlity origin" value="{{ old('origin') }}">
            <span class="text-danger">{{ $errors->first('origin') }}</span>
        </div>


    </div>
    <div class="form-group">
        <div class="col-md-1">
            <label>Photo</label>
        </div>
        <div class="col-md-11">

            <input type="file" id="photo_id" name="photo_id" class="form-control" value="{{ old('photo_id') }}">
            <span class="text-danger">{{ $errors->first('photo_id') }}</span>
        </div>


    </div>
    <div class="form-group">
        <div class="col-md-1">
        </div>
        <div class="col-md-11">
            <button class="btn btn-default btn-danger" onclick="deleteNotifier()">Create</button>
        </div>


    </div>



</form>



<div class="row">
    <div class="col-lg-6 col-sm-offset-5">

    </div>

</div>


@endsection

@section('custom_js')
<script type="text/javascript" src="{{asset('js/bootstrap-datetimepicker.js')}}" charset="UTF-8"></script>
<script type="text/javascript" src="{{asset('js/bootstrap-datetimepicker.fr.js')}}" charset="UTF-8"></script>
<script type="text/javascript">
                $('.form_datetime').datetimepicker({
                    //language:  'fr',
                    weekStart: 1,
                    todayBtn: 1,
                    autoclose: 1,
                    todayHighlight: 1,
                    startView: 2,
                    forceParse: 0,
                    showMeridian: 1
                });
                $('.form_date').datetimepicker({
                    language: 'fr',
                    weekStart: 1,
                    todayBtn: 1,
                    autoclose: 1,
                    todayHighlight: 1,
                    startView: 2,
                    minView: 2,
                    forceParse: 0
                });
                $('.form_time').datetimepicker({
                    language: 'fr',
                    weekStart: 1,
                    todayBtn: 1,
                    autoclose: 1,
                    todayHighlight: 1,
                    startView: 1,
                    minView: 0,
                    maxView: 1,
                    forceParse: 0
                });
</script>

@stop

