@extends('layouts.admin')

@if (session('alert'))
<div class="alert alert-success">
    <?php
    echo session('alert');
    ?>
</div>
@endif
@section('content')

@include('includes.tinyeditor')
<div class="container">
    <h1 style="text-align: center">Edit Product</h1>



    <form action="{{route('admin.products.update',$product->id)}}" method="post" class="form-horizontal" enctype='multipart/form-data'>
        <input type="hidden" name="_token" value="{{ csrf_token() }}"/>
        <input type="hidden" name="_method" value="PUT">
        <div class="form-group">
            <div class="col-md-1">
                <label>name</label>
            </div>
            <div class="col-md-11">
                <input type="text" class="form-control" name="name"  placeholder="Please enter book name" value="{{$product->name}}">
                <span class="text-danger">{{ $errors->first('name') }}</span>
            </div>


        </div>
        <div class="form-group">
            <div class="col-md-1">
                <label>Description</label>
            </div>
            <div class="col-md-11">
                <textarea class="form-control" rows="5" id="body" name="description" value="{{$product->description}}" >{{$product->description}}</textarea>
                <span class="text-danger">{{ $errors->first('description') }}</span>
            </div>


        </div>
        <div class="form-group">
            <div class="col-md-1">
                <label>Type</label>
            </div>
            <div class="col-md-11">

                <select class="form-control" id="type_id" name="type_id" >

                    @foreach ($types as $id => $name  )
                    <option value="{{$id}}" {{$id == $product->type->id ? 'selected="selected"':''}}> {{$name}}</option>
                    @endforeach 

                    <option value="">create other type</option>

                </select>   
                <span class="text-danger">{{ $errors->first('type_id') }}</span>
            </div>


        </div>
        <div class="form-group">
            <div class="col-md-1">
                <label>Author</label>
            </div>
            <div class="col-md-11">
                <select class="form-control" id="author_id" name="author_id" >
                    @foreach ($authors as $id => $name  )
                    <option value="{{$id}}" {{$id == $product->author->id ? 'selected="selected"':''}}> {{$name}}</option>
                    @endforeach 

                    <option value="">create other author</option>
                </select>   
                <span class="text-danger">{{ $errors->first('author_id') }}</span>
            </div>


        </div>
        <div class="form-group">
            <div class="col-md-1">
                <label>Language</label>
            </div>
            <div class="col-md-11">
                <select class="form-control" id="language_id" name="language_id" >
                    @foreach ($languages as $id => $name  )
                    <option value="{{$id}}" {{$id == $product->language->id ? 'selected="selected"':''}}> {{$name}}</option>
                    @endforeach 

                    <option value="">create other language</option>
                </select>   
                <span class="text-danger">{{ $errors->first('language_id') }}</span>
            </div>


        </div>
        <div class="form-group">
            <div class="col-md-1">
                <label>Publisher</label>
            </div>
            <div class="col-md-11">
                <select class="form-control" id="publisher_id" name="publisher_id" >
                    @foreach ($publishers as $id => $name  )
                    <option value="{{$id}}" {{$id == $product->publisher->id ? 'selected="selected"':''}}> {{$name}}</option>
                    @endforeach 

                    <option value="">create other publisher</option>
                </select>   
                <span class="text-danger">{{ $errors->first('publisher_id') }}</span>
            </div>


        </div>
        <div class="form-group">
            <div class="col-md-1">
                <label>Page</label>
            </div>
            <div class="col-md-11">
                <input type="number" class="form-control" name="page"  placeholder="Please enter book numbers" value="{{$product->page}}"></input>
                <span class="text-danger">{{ $errors->first('page') }}</span>
            </div>


        </div>
        <div class="form-group">
            <div class="col-md-1">
                <label>Dimensions</label>
            </div>

            <div class="col-md-11">
                <div class="col-md-3"> <label>{{$product->dimensions}}</label></div>
                <div class="col-md-2" style="background: #1f7d98;    color: white;">   <input type="number" class="form-control" name="width"  placeholder="Please enter book Width" value=""></input></div> 
                <div class="col-md-2" style="background: #1f7d98;    color: white;"> <input type="number" class="form-control" name="height"  placeholder="Please enter book Heigh" value=""></input>

                </div>   <span class="text-danger">{{ $errors->first('quanlity_origin') }}</span>
            </div>

        </div>



        <div class="form-group">
            <div class="col-md-1">
                <label>Publiction date</label>
            </div>
            <div class="col-md-11">

                <fieldset>
                    <div class="control-group">

                        <div class="controls input-append date form_date" data-date="" data-date-format="dd MM yyyy" data-link-field="dtp_input2" data-link-format="yyyy-mm-dd">
                            <input size="16" type="text" name="publication_date" value="{{$product->publication_date}}" readonly>
                            <span class="add-on"><i class="icon-remove"></i></span>
                            <span class="add-on"><i class="icon-th"></i></span>
                            <span class="text-danger">{{ $errors->first('publication_date') }}</span>
                        </div>
                    </div>
                </fieldset>

            </div>
        </div>
        <div class="form-group">
            <div class="col-md-1">
                <label>Orgin price</label>
            </div>
            <div class="col-md-11">

                <input type="number" name="origin_price"class="form-control" value="{{$product->origin_price}}" placeholder="Please enter orgin price">
                <span class="text-danger">{{ $errors->first('origin_price') }}</span>
            </div>


        </div>
        <div class="form-group">
            <div class="col-md-1">
                <label>Sale price</label>
            </div>
            <div class="col-md-11">

                <input type="number" name="promotion_price"class="form-control" value="{{$product->promotion_price}}" placeholder="Please enter promotion price">
                <span>%</span>
                <span class="text-danger">{{ $errors->first('promotion_price') }}</span>
            </div>


        </div>
        <div class="form-group">
            <div class="col-md-1">
                <label>Final price</label>
            </div>
            <div class="col-md-11">
                <input type="number" name="final_price" class="form-control" placeholder="Please enter final price" value="{{$product->final_price}}">
                <span class="text-danger">{{ $errors->first('final_price') }}</span>
            </div>


        </div>
        <div class="form-group">
            <div class="col-md-1">
                <label>Quantity origin</label>
            </div>
            <div class="col-md-11">
                <div class="col-md-2">  <label>{{$product->origin}}</label></div>
                <div class="col-md-2" style="background: #1f7d98;    color: white;">  <label>  insert quantities: </label></div> 
                <div class="col-md-3">     <input type="number" name="quanlity_origin" class="form-control" placeholder="Please enter quanlity origin" value="">
                </div>   <span class="text-danger">{{ $errors->first('quanlity_origin') }}</span>
            </div>


        </div>


        <div class="form-group">
            <div class="col-md-1">
                <label>Photo</label>
            </div>
            <div class="col-md-11">
                <table class="table">

                    @foreach($product->pictures as $picture)
                    <thead>
                        <tr>
                            <th class="form-group " style="width:  200px; height: 200px" >
                                <img src="{{$picture["url"] ? asset( $picture["url"] ): 'http://placehold.it/400x400'}}" height="300" alt="" class="img-responsive img-rounded">
                            </th>
                            <th> 

                                 <button type="button" class="btn btn-primary" id="delete_image" value="{{$product->id}}">Delete image</button>

                            </th>

                            @endforeach 

                        </tr>


                    </thead>
                </table>
                <br>

                <br>
                <input type="file" id="photo_id" name="photo_id" class="form-control" value="{{ old('photo_id') }}" value="{{$product->pictures->first()["url"]}}">
                <span class="text-danger">{{ $errors->first('photo_id') }}</span>
            </div>


        </div>
        <div class="form-group">
            <div class="col-md-1">
            </div>
            <div class="form-group">
                <input type="submit" class="btn btn-success" value="Edit " />
            </div>

        </div>

    </form>
</div>





<div class="row">
    <div class="col-lg-6 col-sm-offset-5">
    </div>

</div>
</div>



<script type="text/javascript" src="{{asset('.js/jquery/jquery-1.8.3.min.js')}}" charset="UTF-8"></script>
<script type="text/javascript" src="{{asset('.css/bootstrap/js/bootstrap.min.js')}}"></script>
<script type="text/javascript" src="{{asset('js/bootstrap-datetimepicker.js')}}" charset="UTF-8"></script>
<script type="text/javascript" src="{{asset('js/bootstrap-datetimepicker.fr.js')}}" charset="UTF-8"></script>
<script type="text/javascript">
$('.form_datetime').datetimepicker({
    //language:  'fr',
    weekStart: 1,
    todayBtn: 1,
    autoclose: 1,
    todayHighlight: 1,
    startView: 2,
    forceParse: 0,
    showMeridian: 1
});
$('.form_date').datetimepicker({
    language: 'fr',
    weekStart: 1,
    todayBtn: 1,
    autoclose: 1,
    todayHighlight: 1,
    startView: 2,
    minView: 2,
    forceParse: 0
});
$('.form_time').datetimepicker({
    language: 'fr',
    weekStart: 1,
    todayBtn: 1,
    autoclose: 1,
    todayHighlight: 1,
    startView: 1,
    minView: 0,
    maxView: 1,
    forceParse: 0
});
</script>

@stop

