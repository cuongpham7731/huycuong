@extends('layouts.admin')
@section('content')


    <h1>Add Admin</h1>
   <div class="row">
        @include('includes.form_error')
    </div>

    <div class="container">
         <div class="row">
         
        <form action="{{ route('admin.users.store') }}" method="post" enctype='multipart/form-data' >
            <input type="hidden" name="_token" value="{{ csrf_token() }}"/>
            <div class=" form-group {{ $errors->has('name') ? 'has-error' : '' }}">
                    <label for="name">UserName:</label>
                    <input type="text" id="name" name="name" class="form-control" placeholder="Enter Your Name" value="{{ old('name') }}">
                    <span class="text-danger">{{ $errors->first('name') }}</span>
            </div>   
             <div class=" form-group {{ $errors->has('address') ? 'has-error' : '' }}">
                    <label for="address">Address:</label>
                    <input type="text" id="address" name="address" class="form-control" placeholder="Enter Your Address" value="{{ old('address') }}">
                    <span class="text-danger">{{ $errors->first('address') }}</span>
            </div>   
             <div class=" form-group {{ $errors->has('phone') ? 'has-error' : '' }}">
                    <label for="phone">Phone:</label>
                    <input type="number" id="phone" name="phone" class="form-control" placeholder="Enter Your Phone" value="{{ old('phone') }}">
                    <span class="text-danger">{{ $errors->first('phone') }}</span>
            </div>   
            <div class="  form-group {{ $errors->has('email') ? 'has-error' : '' }}">
                <label for="email">Email:</label>
                <input type="text" id="email" name="email" class="form-control" placeholder="Enter Your Email" value="{{ old('email') }}">
                <span class="text-danger">{{ $errors->first('email') }}</span>
            </div>
            <div class="  form-group {{ $errors->has('password') ? 'has-error' : '' }}">
                <label for="password">Password:</label>
                <input type="password" id="password" name="password" class="form-control" value="{{ old('password') }}" placeholder="Enter Password">
                <span class="text-danger">{{ $errors->first('password') }}</span>
           </div>
            <div class="form-group">
                <input type="submit" class="btn btn-success" value="Add Admin" />
            </div>
        </form>
    </div>    
    </div>





 @stop