
@extends('layouts.admin')
@section('content')
@if (session('alert'))
    <div class="alert alert-success">
        <?php 
        echo session('alert');
        ?>
    </div>
@endif
<div class="row">
    <div class="col-sm-9">
        <h1>Users</h1>
    </div>
    <div class="col-sm-3">
         <form action="{{ route('admin.users.create')}}" >
         <input type="hidden" name="_method" value="CREATE">
         <input type="hidden" name="_token" value="{{ csrf_token() }}"/>
           <div class="form-group">
               <input type="submit" class="btn btn-danger" value="Add Admin" />
             </div>
    </form>
    </div>
</div>

<table class="table">
    <thead>
        <tr>
            <th>Id</th>
            <th>Photo</th>
            <th>Name</th>
            <th>Address</th>
            <th>Email</th>
            <th>Phone</th>
            <th>Create_at</th>
            <th>Update_at</th>
    </thead>
    <tbody>
        @foreach($users as $key=>$user)
        <tr>
            <td>{{$user->id}}</td>
            <td><img width="50px" height="50px" class="img-rounded" src="http://placehold.it/400x400" alt=""></td>
            <td> <a href="{{ url('admin/users/'.$user->id.'/edit') }}"> {{$user->name}} </a> </td>
            <td>{{$user->address}}</td>
            <td>{{$user->email}}</td>
            <td>{{$user->phone}}</td>
            <td>{{$user->created_at}}</td>
            <td>{{$user->updated_at}}</td>
            <td style="width: 50px!important;">
                <form action="{{ route('admin.users.destroy', $user->id) }}" method="POST" >
         <input type="hidden" name="_method" value="DELETE">
         <input type="hidden" name="_token" value="{{ csrf_token() }}"/>
           <div class="form-group">
               <input type="submit" class="btn btn-danger" value="Delete User" />
               
             </div>
    </form>
            </td>  
        </tr>
        @endforeach  
    </tbody>
</table>
@stop