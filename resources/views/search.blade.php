@extends('layouts.layout')
@section('content')
<section id="featuredProduct">
    <div id="myCarouselOne" class="carousel slide">
        <section class="tg-sectionspace tg-haslayout">
            <div class="container">
                <div class="row">
                    <div class="tg-newrelease">
                        <div class="tg-sectionhead">
                            <h3 class="title"><span>Kết quả tìm kiếm cho "{{$keyword}}"</span></h3>
                        </div>
                        <div class="col-xs-12 col-sm-12 col-md-6 col-md-6">
                            <div class="row">
                                <div class="tg-newreleasebooks">
                                    @foreach($products as $product)
                                    <div class="col-xs-4 col-sm-4 col-md-6 col-lg-4">
                                        <div class="tg-postbook">
                                            <figure class="tg-featureimg">
                                                <div class="tg-bookimg">

                                                    <a  href="{{asset('product_detail/'.$product->id)}}">  
                                                        <div class="tg-frontcover"><img id="imageproduct" src="{{$product->pictures->last()["url"]?asset($product->pictures->last()["url"]): 'http://placehold.it/400x400'}}" alt="image description"></div>
                                                        <div class="tg-backcover"><img id="imageproduct" src="{{$product->pictures->last()["url"]?asset($product->pictures->last()["url"]): 'http://placehold.it/400x400'}}" alt="image description"></div>
                                                    </a>
                                                </div>
                                                <a class="tg-btnaddtowishlist add-to-cart" href="javascript:void(0);" value="{{$product->id}}">
                                                    <i class="icon-cart"></i>
                                                    <span>add to cart</span>
                                                </a>
                                            </figure>
                                            <div class="tg-postbookcontent">
                                                <ul class="tg-bookscategories">
                                                    <li><a href="javascript:void(0);">{{$product->type->name}}</a></li>
                                                </ul>
                                                <div class="tg-booktitle">
                                                    <h3><a href="{{asset('product_detail/'.$product->id)}}">{{$product->name}}</a></h3>
                                                </div>
                                                <span class="tg-bookwriter">By: <a href="javascript:void(0);">{{$product->author->name}}</a></span>
                                                <span class="tg-stars"><span></span></span>
                                            </div>
                                        </div>
                                    </div>
                                    @endforeach
                                </div>
                                <div class="tg-newreleasebooks">
                                    @foreach($authors as $author)
                                    <div class="col-xs-4 col-sm-4 col-md-6 col-lg-4">
                                        <div class="item tg-author">
                                            <figure><a href="javascript:void(0);"><img style="height: 204px !important;"src="{{$author->image?asset($author->image): 'http://placehold.it/400x400'}}" alt="image description"></a></figure>
                                            <div class="tg-authorcontent">
                                                <h2><a href="javascript:void(0);">{{$author->name}}</a></h2>
                                                <span>21,658 Published Books</span>
                                                <ul class="tg-socialicons">
                                                    <li class="tg-facebook"><a href="javascript:void(0);"><i class="fa fa-facebook"></i></a></li>
                                                    <li class="tg-twitter"><a href="javascript:void(0);"><i class="fa fa-twitter"></i></a></li>
                                                    <li class="tg-linkedin"><a href="javascript:void(0);"><i class="fa fa-linkedin"></i></a></li>
                                                </ul>
                                            </div>
                                        </div>
                                    </div>
                                    @endforeach
                                </div>

                            </div>
                        </div>
                    </div>
                </div>
            </div>
    </div>

</section>
<section class="tg-sectionspace tg-haslayout">
    <div class="container">
        <div class="row">
            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                <div class="tg-sectionhead">
                    <h2><span>Latest News &amp; Articles</span>What's Hot in The News</h2>
                    <a class="tg-btn" href="javascript:void(0);">View All</a>
                </div>
            </div>
            <div id="tg-postslider" class="tg-postslider tg-blogpost owl-carousel">
                @foreach($newProducts as $product)
                <article class="item tg-post">
                    <figure> 
                        <a  href="{{asset('product_detail/'.$product->id)}}">  
                            <div class="tg-frontcover"><img id="imageproduct" src="{{$product->pictures->last()["url"]?asset($product->pictures->last()["url"]): 'http://placehold.it/400x400'}}" alt="image description"></div>
                            <div class="tg-backcover"><img id="imageproduct" src="{{$product->pictures->last()["url"]?asset($product->pictures->last()["url"]): 'http://placehold.it/400x400'}}" alt="image description"></div>
                        </a>
                    </figure>
                    <div class="tg-postcontent">
                        <ul class="tg-bookscategories">
                            <li><a href="javascript:void(0);">{{$product->type->name}}</a></li>
                        </ul>
                        <div class="tg-themetagbox"><span class="tg-themetag">featured</span></div>
                        <div class="tg-posttitle">
                            <h3><a href="javascript:void(0);">{{$product->name}}</a></h3>
                        </div>
                        <span class="tg-bookwriter">By: <a href="javascript:void(0);">{{$product->author->name}}</a></span>
                        <ul class="tg-postmetadata">
                            <li><a href="javascript:void(0);"><i class="fa fa-comment-o"></i><i>21,415 Comments</i></a></li>
                            <li><a href="javascript:void(0);"><i class="fa fa-eye"></i><i>24,565 Views</i></a></li>
                        </ul>
                    </div>
                </article>
                @endforeach
            </div>
        </div>
    </div>
</section>
@endsection 



