<?php

/*
  |--------------------------------------------------------------------------
  | Web Routes
  |--------------------------------------------------------------------------
  |
  | Here is where you can register web routes for your application. These
  | routes are loaded by the RouteServiceProvider within a group which
  | contains the "web" middleware group. Now create something great!
  |
 */

Route::get('/search', 'HomeController@search')->name('search');
Route::get('/show', 'HomeController@show')->name('search');
Route::resource('/', 'HomeController');
Route::post('/product_detail/commentProduct', 'HomeController@commentProduct')->name('comment_roduct');
Route::post('/all_product/sortProduct', 'HomeController@sortProduct')->name('sort_product');
Route::get('/product_detail/{id}', 'HomeController@productDetail')->name('product_detail');
Route::resource('/home', 'HomeController');
Route::resource('/comment', 'CommentController', array('as' => 'admin'));
Route::post('/product_detail/{id}/comment', 'CommentController@store');
Route::resource('admin/authors', 'AdminAuthorController', array('as' => 'admin'));
Route::resource('admin/languages', 'AdminLanguageController', array('as' => 'admin'));
Route::resource('admin/types', 'AdminTypeController', array('as' => 'admin'));
Route::resource('admin/publishers', 'AdminPublisherController', array('as' => 'admin'));
Route::resource('admin/products', 'AdminProductsController', array('as' => 'admin'));
Route::resource('admin/comment', 'CommentController', array('as' => 'admin'));
Route::resource('admin/users', 'AdminUsersController', array('as' => 'admin'));
Route::resource('admin/search', 'SearchController', array('as' => 'admin'));

Route::get('/author_detail/{id}/','MenuController@menuAuthors')->name('author_detail');
Route::get('/type_detail/{id}/','MenuController@menuTypes')->name('type_detail');
Route::get('/language_detail/{id}/','MenuController@menuLanguages')->name('language_detail');
Route::get('/publisher_detail/{id}/','MenuController@menuPublishers')->name('publisher_detail');


Route::get('/all_product', 'HomeController@allProduct')->name('all_product');
Route::get('/checkout', 'HomeController@checkOut')->name('checkout');
//Route::resource('/comment', 'CommentController' ,array('as'=>'admin'));
//Route::post('/product_detail/{id}/comment', 'CommentController@store');

Route::resource('admin/authors', 'AdminAuthorController',array('as'=>'admin'));
Route::resource('admin/languages', 'AdminLanguageController',array('as'=>'admin'));
Route::resource('admin/types', 'AdminTypeController',array('as'=>'admin'));
Route::resource('admin/publishers', 'AdminPublisherController',array('as'=>'admin'));
Route::resource('admin/products', 'AdminProductsController',array('as'=>'admin'));
Route::resource('admin/users', 'AdminUsersController' ,array('as'=>'admin'));
Route::resource('admin/search', 'SearchController' ,array('as'=>'admin'));

Route::get('admin/products/deletep/{id}', 'AdminProductsController@deletePicture');
Route::resource('user/management', 'UserManagementController');
Route::resource('user/management', 'UserManagementController');
Route::get('user/{id}', 'UserManagementController@show');
Route::get('user/edit/{id}', 'UserManagementController@edit');
Route::put('user/update/{id}', 'UserManagementController@update');
Route::get('admin/{id}', 'UserManagementController@show');
Route::get('/add-to-cart/{id}', [
    'uses' => 'ProductController@getAddToCart',
]);

Route::get('/delete-cart-product/{id}', [
    'uses' => 'ProductController@removeItem',
    'as' => 'product.removeItem'
]);

Route::get('/delete-cart/', [
    'uses' => 'ProductController@removeCart',
]);

Route::post('/shopping-cart', [
    'uses' => 'ProductController@changeProductQuantity',
    'as' => 'product.changeProductQuantity'
]);

Route::get('/shopping-cart', [
    'uses' => 'ProductController@getCart',
    'as' => 'product.shoppingcart'
]);

Route::get('/contact-us', 'HomeController@contactUs')->name('contact_us');
Route::post('/send-feed-back', 'HomeController@sendFeedback')->name('send_feed_back');
Route::get('/about-us', 'HomeController@aboutUs')->name('about_us');

Auth::routes();
