//////////////////////////////////////////////////////////
//                  Ajax add to shopping cart           //
//////////////////////////////////////////////////////////

$(document).ready(function () {

    $(document).on("click", ".add-to-cart", function (e) {

        e.preventDefault();
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
            }
        });
        $.ajax({
            url: "add-to-cart/" + $(this).attr('value'),
            method: 'get',
            data: {
            },
            success: function (result) {
                changeCartInfo(result);
                alert("Your product was added into cart");
            }});
    });




});



function changeCartInfo(result) {
    console.log(result);
    var listItem = result.items;
    var x, y, price, quantity, name, description, count = 0;
    var html = "";
    var cart_link = $('.cart-btn-show').attr('href');
    var cart_foot = '<a class="tg-btnemptycart" href="/delete-cart" id="clear-all-cart">'
            + '<i class="fa fa-trash-o"></i>'
            + '<span>Clear Your Cart</span>'
            + '</a>'
            + '<span class="tg-subtotal">Subtotal: <strong class="cart-price">' + result.totalPrice + '</strong> <trong>$</trong></span>'
            + '<div class="tg-btns">'
            + '<a class="tg-btn tg-active" href="shopping-cart">View Cart</a>'
            + '</div>';

    for (x in listItem) {
        if (count < 4) {
            price = listItem[x].price;
            quantity = listItem[x].quantity;
            for (y in listItem[x]) {
                name = listItem[x][y].name;
                description = listItem[x][y].description;
            }
            html += '<div class="tg-minicarproduct">'
                    + '<figure>'
                    + '<img src="images/products/img-01.jpg" alt="image description">'
                    + '</figure>'
                    + '<div class="tg-minicarproductdata">'
                    + '<h5><a href="javascript:void(0);">' + name + '</a></h5>'
                    + '<h6><a href="javascript:void(0);">$ ' + price + "   " + "X" + quantity + '</a></h6>'
                    + '</div>'
                    + '</div>';
            count++;
        } else {
            html += '<p style="text-align: center"><a href="' + cart_link + '">View more</a></p>';
            break;
        }

    }



    $('.cart-total-quantity').html(result.totalQuantity);
    $('.cart-total-price').html(result.totalPrice);
    $('.tg-minicartbody').html(html);
    $('.tg-minicartfoot').html(cart_foot);
    $('#cartModal').modal({
        show: 'true'
    });

}

//////////////////////////////////////////////////////////
//   Ajax change number of product shopping cart        //
//////////////////////////////////////////////////////////





//////////////////////////////////////////////////////////
//            Ajax delete product of shopping cart      //
//////////////////////////////////////////////////////////


$(document).ready(function () {

    $(document).on("click", ".remove-cart-product", function (e) {
        e.preventDefault();
        var proId = $(this).attr('value');
        var proName = $(this).attr('name');
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
            }
        });
        $.ajax({
            url: "delete-cart-product/" + proId,
            method: 'get',
            data: {
            },
            success: function (result) {
                changeCartInfo(result);
                changeCartBoard(result);
                alert("Your product removed from cart");
            }});
    });
});

function changeCartBoard(result) {
    var listItem = result.items;
    var x, y, price, quantity, name, description, pictures, author, publisher, language, pictures;
    var html = "";
    var link_product_detail = $('#link-to-product-detail').attr('value');
    for (x in listItem) {

        price = listItem[x].price;
        quantity = listItem[x].quantity;
        for (y in listItem[x]) {
            name = listItem[x][y].name;
            author = listItem[x][y].author;
            publisher = listItem[x][y].publisher;
            language = listItem[x][y].language;
            pictures = listItem[x][y].pictures;
            description = listItem[x][y].description;
        }
        html += '<tr class="cart_item first_item address_0 odd dd-product-group">'
                + '<td class="cart_product">'
                + '<a href="' + link_product_detail + "/" + x + '">'
                + '<img alt="Faded Short Sleeve T-shirts" src="">'
                + '</a>'
                + '</td>'
                + '<td class="cart_description">'
                + '<p class="product-name">'
                + '<a href="' + link_product_detail + "/" + x + '">' + name + '</a>'
                + '</p>'
                + '<small>Author: <span>' + author.name + '</span></small>'
                + '<small>Publisher: <span>' + publisher.name + '</span></small>'
                + '<small>Language: <span>' + language.name + '</span></small>'
                + '</td>'
                + '<td data-title="PRICE" class="cart_unit">'
                + '<span class="price">$<strong>' + price + '</strong></span>'
                + '</td>'
                + '<td class="cart_quantity text-center">'
                + '<div class="cart_quantity_button clrfix">'
                + '<a href="javascript:void(0)" class="cart_quantity_down btn btn-default cart-btn-minus" rel="nofollow">-</a>'
                + '<input type="text" name="quantity" pid="' + x + '" value="' + quantity + '" class="cart_quantity_input form-control grey count" autocomplete="off" size="2" disabled max="5" min="1">'
                + '<a href="javascript:void(0)" class="cart_quantity_up btn btn-default cart-btn-plus" rel="nofollow">+</a>'
                + '</div>'
                + '</td>'
                + '<td class="subtotal text-center" data-title="SUBTOTAL">'
                + '<a class="close-btn ddr remove-cart-product" value="' + x + '" href="javascript:void(0);"><i><strong class="text-danger" style="font-size: 20px">X</strong></i></a>'
                + '<p>$ <strong>' + quantity * price + '</strong></p>'
                + '</td>'
                + '</tr>';
    }
    $(".list-cart-product").html(html);

}


// ============================================================================
// btn-plus and btn-minus in "#order-detail-content" table
// ============================================================================

$(document).on('click', '.cart-btn-plus', function () {
    var $count = $(this).parent().find('.count');
    var val = parseInt($count.val(), 10);
    if (val < 5) {
        $count.val(val + 1);
        changeByQuantity($count.attr("pid"), $count.val());
    }
    return false;
});

$(document).on('click', '.cart-btn-minus', function () {
    var $count = $(this).parent().find('.count');
    var val = parseInt($count.val(), 10);
    if (val > 1) {
        $count.val(val - 1);
        changeByQuantity($count.attr("pid"), $count.val());
    }
    return false;
});

function changeByQuantity(pid, pquantity) {


    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
        }
    });
    $.ajax({
        route: "change-cart-quantity/",
        method: 'post',
        data: {
            id: pid,
            quantity: pquantity
        },
        success: function (result) {
            changeCartInfo(result);
            changeCartBoard(result);
            alert("Your change is success");
        }});
}