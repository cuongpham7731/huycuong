<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable {

    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name',
        'email',
        'password',
        'address',
        'phone',
        'image',
        'is_admin',
        'is_thumbnail'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    public function photo() {
        return $this->belongsTo('App\Photo');
    }

    public function isAdmin() {
        if ($this->is_admin == 1 && $this->is_thumbnail == 1) {
            return true;
        }
        return false;
    }
    public function is_active() {
        if ($this->is_admin == 0 && $this->is_thumbnail == 1) {
            return true;
        }
        return false;
    }

}
