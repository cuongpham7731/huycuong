<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Comment extends Model {

    protected $fillable = [
        'content',
        'date_comment',
        'user_id',
        'parent_id',
        'book_id'
    ];

    public function user() {

        return $this->belongsTo('App\User');
    }

    public function book() {


        return $this->belongsTo('App\Book');
    }
   public function children() { 
       return $this->hasMany('App\Comment', 'parent_id', 'id'); 
   }
}
