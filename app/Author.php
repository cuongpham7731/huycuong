<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Author extends Model {
   use FullTextSearch;
    protected $fillable = [
        'name',
        'image',
        'introduce',
        'is_thumbnail'
    ];

    public function books() {
        return $this->hasMany('App\Book');
    }
    protected $searchable = [
        'name'
    ];

}
