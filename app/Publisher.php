<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Publisher extends Model {

    protected $fillable = [
        'name',
        'is_thumbnail'=>0
    ];

}
