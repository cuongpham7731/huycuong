<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Type extends Model {

    protected $fillable = [
        'name',
        'is_thumbnail'=>0
    ];

}
