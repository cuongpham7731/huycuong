<?php

namespace App;

class Cart {

    public $items;
    public $totalQuantity = 0;
    public $totalPrice = 0;
    public $totalProduct = 0;

    public function __construct($oldCart) {
        if ($oldCart) {
            $this->items = $oldCart->items;
            $this->totalQuantity = $oldCart->totalQuantity;
            $this->totalPrice = $oldCart->totalPrice;
            $this->totalProduct = $oldCart->totalProduct;
        } else {
            $this->items = null;
        }
    }

    public function add($item, $id) {
        $storedItem = ['quantity' => 0, 'price' => $item->final_price, 'item' => $item];
        $price = $item->final_price-(($item->promotion_price*$item->final_price)/100  );
        if ($this->items) {
            if (array_key_exists($id, $this->items)) {
                $storedItem = $this->items[$id];
            } else {
                $this->totalProduct++;
            }
        }
        if ($storedItem['quantity'] < 5 && $storedItem['quantity'] <= $item->remain){
             $storedItem['quantity'] ++;
             $this->totalQuantity++;
             $this->totalPrice += $price;
        }
        $storedItem['price'] = $price;
        $storedItem['subprice'] = $item->final_price * $storedItem['quantity'];
        $this->items[$id] = $storedItem;
        
        if ($this->totalQuantity == 1)
            $this->totalProduct = $this->totalQuantity;
        
    }

    public function changeProductQuantity($id, $quantity) {
       $oldItem = $this->items[$id];
       $this->items[$id]['quantity'] = $quantity;
       $this->totalQuantity += ($quantity - $oldItem['quantity']); 
       $this->totalPrice += (($this->items[$id]['quantity'] * $this->items[$id]['price']) - ($oldItem['quantity'] * $oldItem['price']));
       
    }

    public function removeItem($id) {
        $this->totalQuantity -= $this->items[$id]['quantity'];
        $this->totalPrice -= $this->items[$id]['price'];
        $this->totalProduct--;
        unset($this->items[$id]);
    }

}
