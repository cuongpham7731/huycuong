<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Order_product extends Model
{
    protected $fillable = [
        'quantity',
        'price',
        'order_id',
        'book_id'
    ];
    public function book(){


        return $this->belongsTo('App\Book');


    }
    public function order(){


        return $this->belongsTo('App\Order');


    }
}
