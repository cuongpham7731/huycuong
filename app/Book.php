<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Book extends Model {


    use FullTextSearch;

    protected $fillable = [
        'name',
        'description',
        'language_id',
        'author_id',
        'publisher_id',
        'type_id',
        'page',
        'publication_date',
        'dimensions',
        'origin_price',
        'promotion_price',
        'final_price',
        'origin',
        'remain',
        'is_thumbnail' => 0
    ];
    protected $searchable = [
        'name',
        'description'
    ];


    public function language() {

        return $this->belongsTo('App\Language');
    }

    public function author() {


        return $this->belongsTo('App\Author');
    }

    public function publisher() {


        return $this->belongsTo('App\Publisher');
    }

    public function type() {


        return $this->belongsTo('App\Type');
    }

    public function comments() {
        return $this->hasMany('App\comment');
    }

    public function order_products() {
        return $this->hasMany('App\Order_Product');
    }

    public function pictures() {
        return $this->hasMany('App\Picture');
    }

    public function order_details() {
        return $this->hasMany('App\Order_Product');
    }

}
