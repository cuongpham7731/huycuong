<?php

namespace App;

class Product {

    public $items;
    
    public function __construct() {
            $this->items = null;
    }
    
    public function add($item, $id) {
        $storedItem = ['images' => null, 'item' => $item];

        if ($this->items) {
            if (array_key_exists($id, $this->items)) {
                $storedItem = $this->items[$id];
                $storedItem["images"][] = $item->pictures;
            } else {
                $storedItem["item"] = $item;
                $storedItem["images"] = $this->items[$id]["images"];
            }
        }
        $this->items[$id] = $storedItem;
    }

}
