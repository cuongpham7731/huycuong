<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Book;
use App\Language;
use App\Publisher;
use App\Author;
use App\Type;
use App\Picture;
use App\User;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\File;

class AdminProductsController extends Controller {

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function deletePicture($id) {

        $test = File::exists('images/product/date1.PNG');
        echo $test ? "y" : "n";
        //echo $test ? "Xoa thanh cong" : "Xoa that bai";
        //$picture = App\Picture::find(1);
        //$picture.delete();
    }

    public function index() {
        $products = Book::with('pictures')
                ->where('is_thumbnail', '=', '0')
                ->orderBy('books.id', 'desc')
                ->paginate(5);
        $users= User::all();
        return view('admin.products.index', ['products' => $products,'users'=>$users]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create() {
        $types = Type::pluck('name', 'id')->all();
        $languages = Language::pluck('name', 'id')->all();
        $authors = Author::pluck('name', 'id')->all();
        $publishers = Publisher::pluck('name', 'id')->all();


        return view('admin.products.create', compact('types', 'authors', 'languages', 'publishers'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request) {
        $this->validate($request, [
            'name' => 'required|min:3',
            'description' => 'required',
            'type_id' => 'required|not_in:0',
            'language_id' => 'required|not_in:0',
            'publisher_id' => 'required|not_in:0',
            'author_id' => 'required|not_in:0',
            'origin_price' => 'required|regex:/^\d*(\.\d{-})?$/',
            'final_price' => 'required|regex:/^\d*(\.\d{-})?$/',
            'origin' => 'required|regex:/^\d*(\.\d{-})?$/',
                ], [
            'name.required' => ' The name field is require',
            'name.min' => ' The title must be least 5 characters',
            'description.required' => ' The decription field is require',
            'type_id.not_in:0' => 'The type field is require',
            'language_id.not_in:0' => 'The language field is require',
            'author_id.not_in:0' => 'The author field is require',
            'publisher_id.not_in:0' => 'The publisher field is require',
            'origin_price.required' => 'The orign price field is require',
            'origin_price.regex:/^\d*(\.\d{-})?$/ => 0' => ' The orign price must be numeric',
            'origin_price.regex:/^\d*(\.\d{-})?$/ => 0' => ' The orign price must be numeric',
            'final_price.regex:/^\d*(\.\d{-})?$/ => 0' => ' The orign price must be numeric',
            'origin.required' => 'The quantity origin field is require',
            'origin.numeric' => ' The quantity origin must be numeric',
        ]);

        $user = Auth::user();
        if ($user) {
            $input = $request->all();
            $input['remain'] = $request->get('origin');
            $input['page'] = $request->get('page');

          
            $arr = explode(" ", $request->get('publication_date'));
            $date = $arr[2]."-0".$arr[1]."-".$arr[0];
            $input['publication_date'] = $date;
           
            $input['dimensions'] = $request->get('width') . 'X' . $request->get('height');
           
            
            echo var_dump($input);
            $book = new Book();
            if ($book->create($input)) {
                $book = new Book();

                $book_id = DB::getPdo()->lastInsertId();

                if ($file = $request->file('photo_id')) {
                    $year = date('Y');
                    $month = date('m');
                    $day = date('d');
                    $sub_folder = 'products/' . $book_id . '/' . $year . '/' . $month . '/' . $day . '/';
                    $upload_url = 'images/' . $sub_folder;
                    if (!File::exists(public_path() . '/' . $upload_url)) {
                        File::makeDirectory(public_path() . '/' . $upload_url, 0777, true);
                    }
                    $photo = new Picture();
                    $name = time() . $file->getClientOriginalName();
                    $file->move($upload_url, $name);
                    $photo ['url'] = $upload_url . $name;
                    $photo['book_id'] = $book_id;

                    $photo->save();
                }
            }
            $alert = "<script>alert('New book have added successed ');</script>";
  return redirect('/admin/products')->with('alert', $alert);
            echo var_dump($input);
        } else {
            return view("errors.submit-error", ["data" => "Please login as administrator!"]);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id) {

        return view('admin.products.edit');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id) {
        $types = Type::pluck('name', 'id')->all();
        $languages = Language::pluck('name', 'id')->all();
        $authors = Author::pluck('name', 'id')->all();
        $publishers = Publisher::pluck('name', 'id')->all();

        $product = Book::with('pictures')->findOrFail($id);
        return view('admin.products.edit', compact('product', 'types', 'authors', 'languages', 'publishers')
        );
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id) {
        $this->validate($request, [
            'name' => 'required|min:3',
            'description' => 'required',
            'type_id' => 'required|not_in:0',
            'language_id' => 'required|not_in:0',
            'publisher_id' => 'required|not_in:0',
            'author_id' => 'required|not_in:0',
            'origin_price' => 'required',
            'final_price' => 'required',

                ], [
            'name.required' => ' The name field is require',
            'name.min' => ' The title must be least 5 characters',
            'description.required' => ' The decription field is require',
            'type_id.not_in:0' => 'The type field is require',
            'language_id.not_in:0' => 'The language field is require',
            'author_id.not_in:0' => 'The author field is require',
            'publisher_id.not_in:0' => 'The publisher field is require',
            'origin_price.required' => 'The orign price field is require',

        ]);

        $user = Auth::user();
        if ($user) {
            $book = Book::with('pictures')->findOrFail($id);
            $input['remain'] = $request->get('origin');

            $input = $request->all();
            $input['origin'] = $book->origin += $request->get('quanlity_origin');
            $input['remain'] = $book->remain += $request->get('quanlity_origin');


            if ($book->update($input)) {


                if ($file = $request->file('photo_id')) {
                    $year = date('Y');
                    $month = date('m');
                    $day = date('d');
                    File::deleteDirectory(public_path() . 'product/' . $id);
                    $sub_folder = 'product/' . $id . '/' . $year . '/' . $month . '/' . $day . '/';
                    $upload_url = 'images/' . $sub_folder;
                    if (!File::exists(public_path() . '/' . $upload_url)) {
                        File::makeDirectory(public_path() . '/' . $upload_url, 0777, true);
                    }
                    $photo = new Picture();
                    $name = time() . $file->getClientOriginalName();
                    $file->move($upload_url, $name);
                    $photo ['url'] = $upload_url . $name;
                    $photo['book_id'] = $id;

                    $photo->save();
                }
                \Illuminate\Support\Facades\Session::flash('deleted_user', 'The user has been deleted');
            }

            $alert = "<script>alert(' Book have edited successed ');</script>";
            return redirect('/admin/products')->with('alert', $alert);
        } else {
            return view("errors.submit-error", ["data" => "Please login as administrator!"]);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id) {
        $product = Book::findOrFail($id);
        $product['is_thumbnail'] = 1;
        $product->save();
        \Illuminate\Support\Facades\Session::flash('deleted_post', 'The post has been deleted');
        return redirect('/admin/products');
    }

    public function deteltePicture($id) {
        $product = Book::findOrFail($id);

        \Illuminate\Support\Facades\Session::flash('deleted_post', 'The post has been deleted');
        return redirect('/admin/products/edit', $pro);
    }

}
