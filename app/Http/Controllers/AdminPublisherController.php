<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Publisher;

class AdminPublisherController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
       $publishers = Publisher::orderBy('created_at', 'asc')->paginate(5);
        return view('admin.publishers.index', compact('publishers'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
       return view('admin.publishers.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'name' => 'required|unique:publishers|min:3|max:50',
                ], [
            'name.unique' => ' The PublisherName field is exists',
            'name.required' => ' The PublisherName field is require',
            'name.min' => ' The PublisherName must be least 5 characters',
            'name.max' => ' The PublisherName must be maximum 35 characters',
        ]);

        $input = $request->all();
        $status = Publisher::create($input) ? "success" : "fail";

        $message = $status == "success" ? "Success! Your created successful" : "Failed! Your created fail";
        $response = array('status' => $status, 'message' => $message);
        return redirect('/admin/publishers')->with('response', $response);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
         $publisher = Publisher::findOrFail($id);
        return view('admin.publishers.edit', compact('publisher'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
         $this->validate($request, [
            'name' => 'required|min:3|max:50',
                ], [
            'name.required' => ' The AuthorName field is require',
            'name.min' => ' The AuthorName must be least 5 characters',
            'name.max' => ' The AuthorName must be maximum 35 characters',
        ]);
        $publisher = Publisher::findOrFail($id);
        $input = $request->all();
        $status = $publisher->update($input) ? "success" : "fail";

        $message = $status == "success" ? "Success! Your updated successful" : "Failed! Your updated fail";
        $response = array('status' => $status, 'message' => $message);
        return redirect('/admin/publishers')->with('response', $response);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $publisher = Publisher::findOrFail($id);

        $status = $publisher->delete() ? "success" : "fail";

        $message = $status == "success" ? "Success! Your deleted successful" : "Failed! Your deleted fail";
        $response = array('status' => $status, 'message' => $message);
        return redirect('/admin/publishers')->with('response', $response);
    }
}
