<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Type;

class AdminTypeController extends Controller {

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index() {
        $types = Type::orderBy('created_at', 'asc')->paginate(2);
        return view('admin.types.index', compact('types'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create() {
        return view('admin.types.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request) {
        $this->validate($request, [
            'name' => 'required|unique:types|min:3|max:50',
                ], [
            'name.unique' => ' The TypeName field is exists',
            'name.required' => ' The AuthorName field is require',
            'name.min' => ' The AuthorName must be least 5 characters',
            'name.max' => ' The AuthorName must be maximum 35 characters',
        ]);

        $input = $request->all();
        $status = Type::create($input) ? "success" : "fail";

        $message = $status == "success" ? "Success! Your created successful" : "Failed! Your created fail";
        $response = array('status' => $status, 'message' => $message);
        return redirect('/admin/types')->with('response', $response);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id) {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id) {
        $type = Type::findOrFail($id);
        return view('admin.types.edit', compact('type'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id) {
        $this->validate($request, [
            'name' => 'required||unique:types|min:3|max:50',
                ], [
            'name.required' => ' The TypeName field is require',
            'name.min' => ' The TypeName must be least 5 characters',
            'name.max' => ' The TypeName must be maximum 35 characters',
        ]);
        $type = Type::findOrFail($id);
        $input = $request->all();
        $status = $type->update($input) ? "success" : "fail";

        $message = $status == "success" ? "Success! Your updated successful" : "Failed! Your updated fail";
        $response = array('status' => $status, 'message' => $message);
        return redirect('/admin/types')->with('response', $response);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id) {
        $type = Type::findOrFail($id);

        $status = $type->delete() ? "success" : "fail";

        $message = $status == "success" ? "Success! Your deleted successful" : "Failed! Your deleted fail";
        $response = array('status' => $status, 'message' => $message);
        return redirect('/admin/types')->with('response', $response);
    }

}
