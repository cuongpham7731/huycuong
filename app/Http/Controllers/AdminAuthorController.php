<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Author;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\File;

class AdminAuthorController extends Controller {

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index() {


        $authors = Author::orderBy('created_at', 'desc')->paginate(5);

        return view('admin.authors.index', compact('authors'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create() {
        return view('admin.authors.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request) {
        $this->validate($request, [
            'name' => 'required|unique:authors|min:3|max:50',
            'introduce' => 'required|min:3',
                ], [
            'name.unique' => ' The AuthorName field is exists',
            'name.required' => ' The AuthorName field is require',
            'name.min' => ' The AuthorName must be least 3 characters',
            'name.max' => ' The AuthorName must be maximum 35 characters',
            'introduce.min' => ' The introduce author must be least 3 characters',
           
        ]);



        $input = $request->all();

        if ($file = $request->file('photo_id')) {
            $year = date('Y');
            $month = date('m');
            $day = date('d');

            $sub_folder = 'author/' . '/' . $year . '/' . $month . '/' . $day . '/';
            $upload_url = 'images/' . $sub_folder;
            if (!File::exists(public_path() . '/' . $upload_url)) {
                File::makeDirectory(public_path() . '/' . $upload_url, 0777, true);
            }

            $name = time() . $file->getClientOriginalName();
            $file->move($upload_url, $name);
           
            $input['image'] = $upload_url . $name;
        }

        $status = Author::create($input) ? "success" : "fail";

        $message = $status == "success" ? "Success! Your created successful" : "Failed! Your created fail";
        $response = array('status' => $status, 'message' => $message);
        return redirect('/admin/authors')->with('response', $response);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id) {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id) {
        $author = Author::findOrFail($id);
        return view('admin.authors.edit', compact('author'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id) {
        $this->validate($request, [
            'name' => 'required|min:3|max:50',
                ], [
            'name.required' => ' The AuthorName field is require',
            'name.min' => ' The AuthorName must be least 5 characters',
            'name.max' => ' The AuthorName must be maximum 35 characters',
        ]);
        
        $input = $request->all();
        
        if ($file = $request->file('photo_id')) {
            $year = date('Y');
            $month = date('m');
            $day = date('d');

            $sub_folder = 'author' . '/' . $year . '/' . $month . '/' . $day . '/';
            $upload_url = 'images/' . $sub_folder;
            if (!File::exists(public_path() . '/' . $upload_url)) {
                File::makeDirectory(public_path() . '/' . $upload_url, 0777, true);
            }

            $name = time() . $file->getClientOriginalName();
            $file->move($upload_url, $name);
            $input['image'] = $upload_url . $name;
          
             
        }
        $author = Author::findOrFail($id);
        $status = $author->update($input) ? "success" : "fail";
        $message = $status == "success" ? "Success! Your updated successful" : "Failed! Your updated fail";
        $response = array('status' => $status, 'message' => $message);
        return redirect('/admin/authors')->with('response', $response);
        
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id) {
        $author = Author::findOrFail($id);

        $status = $author->delete() ? "success" : "fail";

        $message = $status == "success" ? "Success! Your deleted successful" : "Failed! Your deleted fail";
        $response = array('status' => $status, 'message' => $message);
        return redirect('/admin/authors')->with('response', $response);
    }

}
