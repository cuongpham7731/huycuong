<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Session;
use App\Book;
use App\Cart;

class ProductController extends Controller {

    public function getCart() {
        if (!Session::has('cart')) {
            $types = \App\Type::all();
            $languages = \App\Language::all();
            $publishers = \App\Publisher::all();
            $authors = \App\Author::all();
            return view('shop.cart', ['products' => null], compact('types', 'languages', 'publishers', 'authors'));
        }
        $oldCart = Session::get('cart');
        $cart = new Cart($oldCart);
        $types = \App\Type::all();
        $languages = \App\Language::all();
        $publishers = \App\Publisher::all();
        $authors = \App\Author::all();
        return view('shop.cart', ['products' => $cart->items, 'totalPrice' => $cart->totalPrice], compact('types', 'languages', 'publishers', 'authors'));
    }

    public function getIndex() {
        $products = Book::orderBy('created_at', 'ASC') - get()->paginate(12);
        return view('home', compact('products'));
    }

    public function getAddToCart($id) {
        $product = Book::with('pictures')->findOrFail($id);
        $oldCart = Session::has('cart') ? Session::get('cart') : null;
        $cart = new Cart($oldCart);
        $cart->add($product, $product->id);
        session()->put('cart', $cart);
        return response()->json(Session::get('cart'));
    }

    public function getCheckout() {
        if (!Session::has('cart')) {
            return view('cart', ['products' => null]);
        }
        $oldCart = Session::get('cart');
        $cart = new Cart($oldCart);
        $total = $cart->totalPrice;
        return view('checkout', ['total' => $total]);
    }

    public function changeProductQuantity(Request $request) {

        $oldCart = Session::has('cart') ? Session::get('cart') : null;
        $cart = new Cart($oldCart);
        $cart->changeProductQuantity($request->id, $request->quantity);
        session()->put('cart', $cart);
        return response()->json(Session::get('cart'));
    }

    
    public function removeItem($id) {
        $oldCart = Session::has('cart') ? Session::get('cart') : null;
        $cart = new Cart($oldCart);
        $cart->removeItem($id);
        if (count($cart->items) > 0) {
            Session::put('cart', $cart);
        } else {
            Session::forget('cart');
        }
        return response()->json(session()->has('cart') ? session()->get('cart') : null);
    }

    public function removeCart() {
        $remove = session()->pull('cart', 'default');
        return back()->withInput();
    }

}
