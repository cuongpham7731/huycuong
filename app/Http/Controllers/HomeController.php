<?php

namespace App\Http\Controllers;

use App\Comment;
use App\Book;
use App\Language;
use App\Publisher;
use App\Author;
use App\Picture;
use App\User;
use App\Type;
use App\Order_product;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use App\Mail\SendFeedback;

class HomeController extends Controller {

    public function index() {
        
//        $listBestSelling = Order_product::groupBy('book_id')
//                ->orderByRaw('SUM(order_products.quantity) DESC')
//                ->skip(10)->take(5)->get();
        $listBestSelling = DB::select('SELECT book_id, SUM(order_products.quantity) AS total '
                . 'FROM order_products GROUP BY book_id ORDER BY SUM(order_products.quantity) DESC LIMIT 10');
        $bestSellProducts = null;
        
        foreach ($listBestSelling as $key => $product) {
            $bestSellProducts[] = Book::where('id' ,$product->book_id)
                                    ->with("pictures")
                                    ->groupBy('id')
                                    ->get();

        }
        //echo var_dump($bestSellProducts[0]->first()->pictures->first()->url);
        $types = Type::all();
        $authors = Author::all();
        $languages = Language::all();
        $publishers = Publisher::all();
        $newProducts = Book::with('pictures')
                ->where('is_thumbnail', '=', '0')
                ->orderBy('created_at', 'desc')
                ->paginate(3);

        $randomProducts = Book::with('pictures')
                ->where('is_thumbnail', '=', '0')
                ->inRandomOrder()
                ->get();
        $numberProduct = Book::sum('origin');
        $numberUser = User::all();
        $numberproductsale = Order_product::sum('quantity');
        $saleProducts = Book::with('pictures')
                ->orderBy('promotion_price', 'desc')
                ->inRandomOrder()
                ->get();
        return view('home', compact('bestSellProducts', 'numberUser', 'saleProducts', 'numberproductsale', 'numberProduct', 'newProducts', 'randomProducts', 'types', 'authors', 'languages', 'publishers'));
    }

    public function productDetail($id) {
        $types = Type::all();
        $authors = Author::all();
        $image = Picture::where('book_id', '=', $id)->orderBy('created_at', 'desc')->paginate(4);
        $languages = Language::all();
        $publishers = Publisher::all();
        $user = Auth::user();
        $comments = Comment::where('book_id', '=', $id)->orderBy('created_at', 'desc')->paginate(6);
        $type = Type::pluck('name', 'id')->all();
        $language = Language::pluck('name', 'id')->all();
        $author = Author::pluck('name', 'id')->all();
        $publisher = Publisher::pluck('name', 'id')->all();
        $product = Book::with('pictures')->findOrFail($id);
        $getidauthor = $product->author_id;
        $proByAuthor = Book::with('pictures')->orderBy('created_at', 'desc')->where('author_id', '=', $getidauthor)->paginate(4);
        $allproducts = Book::with('pictures')
                        ->where('is_thumbnail', '=', '0')
                        ->orderBy('books.id', 'desc')->paginate();
        return view('product_detail', compact('comments', 'user', 'image', 'product', 'allproducts', 'types', 'proByAuthor', 'authors', 'languages', 'publishers', 'type', 'author', 'language', 'publisher'));
    }

    public function commentProduct(Request $request) {
        if (($request->get('comment_content') != '') || ($request->get('book_id') != '') || ($request->get('parent_id') != '')) {
            $user = Auth::user();
            if ($user) {
                $comment = new Comment();
                $comment->book_id = $request->get('book_id');
                $comment->parent_id = $request->get('parent_id');

                $comment->user_id = $user->id;

                $comment->content = $request->get('comment_content');
                $comment->save();
                return response()->json($comment);
            } else {
                return view("errors.submit-error", ["data" => "Please login as administrator!"]);
            }
            return response()->json(FALSE);
        }
    }

    public function contactUs() {
        return view('shop.contact');
    }

    public function checkOut() {
        return view('checkout');
    }

    public function aboutUs() {
        return view('shop.about');
    }

    public function sendFeedback(Request $request) {
        $mailTo = "huycuong333@gmail.com";
        $mailFrom = $request->email;
        $subject = "Feedback from " . $request->name;
        $message = "<div>Hello Admin,</div>"
                . "<br><div>A product with name is " . $request->name
                . " and his or her email mail is <a href='mailto:"
                . $request->email . "' >"
                . $request->email . "</a></div>"
                . "<br><div>The feedback is below</div>"
                . "<br>"
                . $request->content;

        $send = new SendFeedback($mailTo, $mailFrom, $subject, $message);
        $send->sendMail();
        return redirect('/contact-us')->with('message', $message);
    }

    public function allProduct(Request $request) {
        $allproducts = Book::with('pictures')
                ->groupBy('id')
                ->paginate(12)
        ;
        $authors = Book::with('pictures')->orderBy('created_at', 'desc')->paginate(4);
        return view('all_product', compact('allproducts', 'authors'));
    }

    public function sortProduct(Request $request) {
        $allproducts = $request->get('sort') == 1 ? Book::with('pictures', 'author', 'language', 'type', 'publisher')->orderByRaw('(final_price - promotion_price) desc')->get() : Book::with('pictures', 'author', 'language', 'type', 'publisher')->orderByRaw('(final_price - promotion_price) asc')->get();

        return response()->json($allproducts);
    }

    public function search(Request $request) {
        $keyword = $request->search;
        $products = Book::whereRaw("MATCH(name, description) AGAINST(? IN BOOLEAN MODE)", $keyword)->get();
        $authors = Author::whereRaw("MATCH(name) AGAINST(? IN BOOLEAN MODE)", $keyword)->get();
        return view('result', ['products' => $products, 'authors' => $authors, 'keyword' => $keyword]);
    }

    public function show(Request $request) {
        $keyword = $request->search;
        $products = Book::whereRaw("MATCH(name, description) AGAINST(? IN BOOLEAN MODE)", $keyword)->get();
        $authors = Author::whereRaw("MATCH(name) AGAINST(? IN BOOLEAN MODE)", $keyword)->get();
        $newProducts = Book::with('pictures')
                ->where('is_thumbnail', '=', '0')
                ->orderBy('created_at', 'desc')
                ->paginate(5);
        return view('search', ['products' => $products, 'authors' => $authors, 'newProducts' => $newProducts, 'keyword' => $keyword]);
    }

}
